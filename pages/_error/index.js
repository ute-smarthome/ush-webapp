import React from 'react'
import './style.scss'
import { connect } from 'react-redux'
class Error404Screen extends React.PureComponent {
  render () {
    return (
      <div className='background-color-from-global text-color-from-global'>
          This is custom error page
      </div>
    )
  }
}

const mapStateToProps = state => ({
  locale: state.locale
})

export default connect(mapStateToProps)(Error404Screen)

import React from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
import { Layout, Row, Col } from 'antd'
import { images } from 'config/images'
import SelectLanguage from 'pages/Components/SelectLanguage'
import './style.scss'
import { TwitterOutlined } from '@ant-design/icons'
class Footer extends React.PureComponent {
  render () {
    const { messages } = this.props.locale
    const { userRedux } = this.props
    return userRedux ? (
      <Layout.Footer className='footer-container'>
        <div className='container'>
          <Row>
            <Col xs={24} lg={5}>
              <img className='logo-footer' src={images.logoFooter} alt='USH LOGO FOOTER' />
            </Col>
            <Col xs={24} lg={13}>
              <Row className='MT70'>
                <Col xs={24} lg={8} className='flex align-center'>
                  <img src={images.icTwitter} alt='Twitter' className='social-icon' />
                  Twitter</Col>
                <Col xs={24} lg={8} className='flex align-center'>
                  <img src={images.icTelegram} alt='Telegram' className='social-icon' />
                  Telegram</Col>
                <Col xs={24} lg={8} className='flex align-center'>
                  <img src={images.icContactUs} alt='Contact Us' className='social-icon' />
                  Contact with us</Col>
              </Row>
              <div className='footer__copyright-text'>
                  CopyRight &copy;
              </div>
            </Col>
            <Col xs={24} lg={6}>
              <div className='footer__btn-appstore MT30'>
                <a href='http://appstore.apple.com'>
                  <img src={images.btnApple} alt='' />
                </a>
              </div>
              <div className='footer__btn-ggplay'>
                <a href='http://play.google.com'>
                  <img src={images.btnGoogle} alt='' />
                </a>
              </div>
            </Col>
          </Row>
        </div>
      </Layout.Footer>
    ) : ''
  }
}
const mapStateToProps = state => ({
  locale: state.locale,
  userRedux: state.userRedux
})

export default connect(mapStateToProps)(Footer)

import React from 'react'
import { images } from 'config/images'
import './style.scss'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import StorageAction from 'controller/Redux/actions/storageActions'
import { withRouter } from 'next/router'
import jaLocale from 'moment/locale/ja'
import enLocale from 'moment/locale/en-au'
import moment from 'moment'
import copy from 'copy-to-clipboard'
import { showNotification, getNameObject } from 'common/function'
import { UnorderedListOutlined, HomeFilled, SearchOutlined } from '@ant-design/icons'
import { Layout, Menu, Drawer, Form, Input, Dropdown } from 'antd'
import Media from 'react-media'
import Link from 'next/link'
import SelectLanguage from 'pages/Components/SelectLanguage'
import MyModal from 'pages/Components/MyModal'
import MyButton from 'pages/Components/MyButton'
import { get } from 'lodash'
import ReduxServices from 'common/redux'

const configRenderLanguage = [
  {
    title: 'United States Of America',
    lang: 'en',
    src: images.flagEnglish
  },
  {
    title: '日本語',
    lang: 'ja',
    src: images.flagJapan
  },
  {
    title: '中文',
    lang: 'cn',
    src: images.flagChina
  },
  {
    title: 'Vietnam',
    lang: 'vi',
    src: images.flagVietnam
  }
]
class Header extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: false,
      visible: false,
      scrolled: false
    }
    this.myModal = React.createRef()
  }

  showDrawer = () => {
    this.setState({
      visible: true
    })
  };

  onClose = () => {
    this.setState({
      visible: false
    })
  };

  onSetLocale = lang => () => {
    const { setLocale } = this.props
    setLocale && setLocale(lang)
    switch (lang) {
    case 'en':
      moment.updateLocale('en', enLocale, {
        monthsShort: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec'
        ]
      })
      break
    case 'ja':
      moment.updateLocale('ja', jaLocale, {
        monthsShort: [
          '1月',
          '2月',
          '3月',
          '4月',
          '5月',
          '6月',
          '7月',
          '8月',
          '9月',
          '10月',
          '11月',
          '12月'
        ]
      })
      moment.updateLocale('ja', {
        weekdays: ['(日)', '(月)', '(火)', '(水)', '(木)', '(金)', '(土)']
      })
      break
    case 'cn':
      moment.updateLocale('ja', jaLocale, {
        monthsShort: [
          '1月',
          '2月',
          '3月',
          '4月',
          '5月',
          '6月',
          '7月',
          '8月',
          '9月',
          '10月',
          '11月',
          '12月'
        ]
      })
      moment.updateLocale('ja', {
        weekdays: [
          '星期天',
          '星期一',
          '星期二',
          '星期三',
          '星期四',
          '星期五',
          '星期六'
        ]
      })
      break
    }
  };

  handleLogout = () => {
    const { router, userRedux } = this.props
    if (userRedux) router.push('/authentication')
    return ReduxServices.resetReduxData()
  }

  handleChangeRoute = (pathname, query = {}) => () => {
    const { router } = this.props
    return router.push({
      pathname,
      query
    })
  }

  menu = () => {
    const { messages } = this.props.locale
    return (
      <React.Fragment>
        <Menu
          theme='dark'
          mode='vertical'
          className='user-navigation'
          style={{ lineHeight: '100px',
            padding: '15px',
            borderRadius: '1.2rem',
            fontSize: '1.6rem',
            minWidth: '35.6rem'
          }}
          selectedKeys={[null]}
        >
          {/* <Menu.Item>
            <div className='header-icon'>
              <img src={images.icPinCode} alt='' />
            </div>
            {messages.pinCode}
          </Menu.Item>
          <Menu.Item>
            <div className='header-icon'>
              <img src={images.icBell} alt='' />
            </div>
            {messages.notification}
          </Menu.Item>
          <Menu.Item>
            <div className='header-icon'>
              <img src={images.icLock} alt='' />
            </div>
            {messages.twoFA}
          </Menu.Item> */}
          <Menu.Item onClick={this.handleChangeRoute('/account')}>
            Thông tin cá nhân
          </Menu.Item>
          <Menu.Item onClick={this.handleLogout}>
            Đăng xuất
          </Menu.Item>
        </Menu>
      </React.Fragment>
    )
  };

  copyAddress = e => {
    e.preventDefault()
    e.stopPropagation()
    const { messages } = this.props.locale
    copy('OK')
    showNotification(messages.copyAddress)
  };

  renderDeskopNavBar () {
    const { userRedux } = this.props
    const { messages } = this.props.locale

    const selectedKey = this.props.router.asPath.replace('/', '') || 'home'
    return (
      <Layout.Header className='header'>
        <div className='header__logo'>
          <Link href='/'>
            <img src={images.logoHorizontal} />
          </Link>
        </div>
        <Menu className='header__nav' selectedKeys={[selectedKey]} mode='horizontal'>
          <Menu.Item key='home'>
            <Link href='/'>
              <span>
                <img src={images[selectedKey === 'home' ? 'navHomeActive' : 'navHome']} alt='' />
                Trang Chủ
              </span>
            </Link>
          </Menu.Item>
          <Menu.Item key='manager'>
            <Link href='/manager'>
              <span>
                <img src={images[selectedKey === 'manager' ? 'navMarketActive' : 'navMarket']} alt='' />
                Quản Lý
              </span>
            </Link>
          </Menu.Item>
          {/* <Menu.Item key='wallet'>
            <Link href='/wallet'>
              <span>
                <img src={images[selectedKey === 'wallet' ? 'navWalletActive' : 'navWallet']} alt='' />
                {messages.wallet}
              </span>
            </Link>
          </Menu.Item> */}
          <Menu.Item key='helpAndSupport'>
            <Link href='/helpAndSupport'>
              <span>
                <img src={images[selectedKey === 'helpAndSupport' ? 'navMarketActive' : 'navMarket']} alt='' />
                Liên hệ
              </span>
            </Link>
          </Menu.Item>
        </Menu>
        <Form name='header_search_form' className='header__search-form'>
          <Form.Item style={{ marginBottom: 0 }}>
            <SearchOutlined />
            <Input placeholder='Tìm kiếm' />
          </Form.Item>
        </Form>
        {/* <SelectLanguage /> */}
        <div className='header-authentication'>
          {userRedux ? (
            <Dropdown overlay={this.menu}>
              <div className='header__user'>
                <span>{get(userRedux, ['name'])}</span>
                <div className='header__user__avatar'>
                  <img src={get(userRedux, ['image']) ? `${process.env.REACT_APP_IMAGE}${get(userRedux, ['image'])}` : images.defaultUser} alt='' />
                </div>
              </div>
            </Dropdown>
          ) : (
            <div className='flex'>
              <MyButton onClick={this.handleChangeRoute('/authentication')} className='header-authentication__button MR8' transparent>{messages.signIn}</MyButton>
              <MyButton onClick={this.handleChangeRoute('/authentication', { action: 'signUp' })} className='header-authentication__button'>{messages.signUp}</MyButton>
            </div>
          )}

        </div>
      </Layout.Header>
    )
  }

  renderMobleNavBar () {
    const { messages } = this.props.locale
    let { lang } = this.props.locale
    let currentSelectedObj = configRenderLanguage.find(
      obj => obj.lang === lang
    )
    return (
      <div>
        <Drawer
          title=''
          placement='right'
          onClose={this.onClose}
          visible={this.state.visible}
          drawerStyle={{ background: '#020815' }}
        >
          <Menu
            theme='dark'
            mode='vertical'
            style={{ lineHeight: '64px', background: '#020815' }}
          >
            <Menu.Item>
              <Link href='/'>{'Trang Chủ'}</Link>
            </Menu.Item>
            <Menu.Item>
              <Link href='/manager'>{'Quản Lý'}</Link>
            </Menu.Item>
            <Menu.Item>
              <Link href='/helpAndSupport'>{'Liên hệ'}</Link>
            </Menu.Item>
            {/* <Menu.Item>
              <a onClick={this.onShowContactPopup}>{messages.contactUs || ''}</a>
            </Menu.Item> */}
            {/* <Menu.Item key='2' title='submenu'>
              <SelectLanguage />
            </Menu.Item> */}
          </Menu>
        </Drawer>
        <Layout.Header className='header-container'>
          <Menu
            theme='dark'
            mode='horizontal'
            style={{ lineHeight: '64px', background: '#020815' }}
          >
            <Menu.Item key='1'>
              <div className='logo'>
                USH-Smarthome
                {/* <img src={images.logoHeader} alt='USH smarthome' /> */}
              </div>
            </Menu.Item>
            <Menu.Item
              style={{
                float: 'right',
                display: 'flex',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}
            >
              <a onClick={this.showDrawer}>
                <UnorderedListOutlined
                  style={{ fontSize: 20, color: 'white' }}
                />
              </a>
            </Menu.Item>
            <Menu.Item style={{ float: 'right', paddingLeft: 0, paddingRight: 0 }}>
              <a href='#' onClick={this.onShowLoginPopup}>{messages.login || ''}</a>
            </Menu.Item>
          </Menu>
        </Layout.Header>
      </div>
    )
  }
  render () {
    const { userRedux } = this.props
    return userRedux ? (
      <React.Fragment>
        <Media
          query='(min-width: 769px)'
          render={() => this.renderDeskopNavBar()}
        />
        <Media
          query='(max-width: 768px)'
          render={() => this.renderMobleNavBar()}
        />
        <MyModal ref={this.myModal} />
      </React.Fragment>
    ) : ''
  }
}
const mapStateToProps = state => ({
  locale: state.locale,
  settingRedux: state.settingRedux,
  internetRedux: state.internetRedux,
  userRedux: state.userRedux,
  globalHeader: state.globalHeader
})
const mapDispatchToProps = dispatch => {
  return {
    setLocale: bindActionCreators(StorageAction.setLocale, dispatch)
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header))

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Detector } from 'common/components/InternetDetect'
import { Layout, Modal } from 'antd'
import Header from './Header'
import Footer from './Footer'
import './style.scss'
import { withRouter } from 'next/router'
const { Content } = Layout

class BaseContainer extends PureComponent {
  // render () {
  //   return (
  //     <Detector
  //       onChange={ReduxService.refreshInternet}
  //       onDefault={ReduxService.refreshInternet}
  //       render={this.renderContainer}
  //     />
  //   )
  // }

  render () {
    const { asPath } = this.props.router
    return (
      <Layout>
        {asPath.includes('authentication') ? null : <Header />}
        <Layout className={`layout-container ${asPath.indexOf('my-page') >= 0 ? 'layout-container--blue' : ''}`}>
          <Content className='base-content'>
            <div className='base-container'>
              {this.props.children}
            </div>
          </Content>
        </Layout>
        {asPath.includes('authentication') ? null : <Footer />}
      </Layout>
    )
  }
}

const mapStateToProps = (state) => ({
  locale: state.locale
})

export default connect(mapStateToProps)(withRouter(BaseContainer))

import React from 'react'
import Link from 'next/link'

import './style.scss'
import { getLength } from 'common/function'

/*
arr = [
    {
        path: '/',
        title: 'title',
    }
]
*/

const Breadcrumb = (props) => {
  const { path } = props
  const { title } = path[getLength(path) - 1]
  return (
    <div className='my-breadcrumb'>
      <ul>
        {path.map((item) => {
          return (
            <li key={item.path}>
              <Link href={item.path}>{item.title}</Link>
            </li>
          )
        })}
      </ul>
      {/* <h2 className='my-breadcrumb__title'>{title}</h2> */}
    </div>
  )
}

export default Breadcrumb

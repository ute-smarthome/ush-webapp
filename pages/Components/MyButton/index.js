import React from 'react'
import './style.scss'
class MyButton extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    const {
      onClick = null,
      title = 'OK',
      isDisabled = false,
      containerCss = '',
      className = '',
      transparent = false,
      isReverse = false,
      isFullWidth = false,
      isNarrow = false,
      isCard = false,
      isSecondary = false
    } = this.props
    return (
      <div className={`button-container ${containerCss}`}>
        <div
          className={`button-content ${isSecondary && 'secondary'} ${(isDisabled && 'disabled') ||
            ''} ${(transparent && 'transparent') || ''} ${isReverse &&
            'reverse'} ${(isFullWidth && 'fullWidth') || ''} ${(isNarrow &&
            'narrow') ||
            ''} ${(isCard && 'isCard') || ''} ${className}`}
          onClick={!isDisabled && onClick}
        >
          {this.props.children || title}
        </div>
      </div>
    )
  }
}

export default MyButton

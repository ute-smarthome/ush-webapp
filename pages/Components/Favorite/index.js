import React from 'react'
import { StarFilled, StarOutlined } from '@ant-design/icons'
import './style.scss'
const Favorite = ({ isFavorite = false, onChange = () => { } }) => {
  return (
    <div className={`my-favorite ${isFavorite ? 'my-favorite--active' : ''}`} onClick={onChange}>
      {isFavorite ? <StarFilled /> : <StarOutlined />}
    </div>
  )
}

export default Favorite

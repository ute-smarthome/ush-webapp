import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from 'moment'
import { Menu, Dropdown } from 'antd'
import jaLocale from 'moment/locale/ja'
import enLocale from 'moment/locale/en-au'
import StorageAction from 'controller/Redux/actions/storageActions'

// Assets
import { images } from 'config/images'
import './style.scss'

const configRenderLanguage = [
  // {
  //   title: '中文',
  //   lang: 'cn',
  //   src: images.flagChina
  // },
  {
    title: 'United States Of America',
    lang: 'en',
    src: images.flagEnglish
  },
  // {
  //   title: '日本語',
  //   lang: 'ja',
  //   src: images.flagJapan
  // },
  {
    title: 'Vietnam',
    lang: 'vi',
    src: images.flagVietnam
  }
]
class SelectLanguage extends React.PureComponent {
  onSetLocale = lang => () => {
    const { setLocale } = this.props
    setLocale && setLocale(lang)
    switch (lang) {
    case 'en':
      moment.updateLocale('en', enLocale, {
        monthsShort: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec'
        ]
      })
      break
    case 'ja':
      moment.updateLocale('ja', jaLocale, {
        monthsShort: [
          '1月',
          '2月',
          '3月',
          '4月',
          '5月',
          '6月',
          '7月',
          '8月',
          '9月',
          '10月',
          '11月',
          '12月'
        ]
      })
      moment.updateLocale('ja', {
        weekdays: ['(日)', '(月)', '(火)', '(水)', '(木)', '(金)', '(土)']
      })
      break
    case 'cn':
      moment.updateLocale('ja', jaLocale, {
        monthsShort: [
          '1月',
          '2月',
          '3月',
          '4月',
          '5月',
          '6月',
          '7月',
          '8月',
          '9月',
          '10月',
          '11月',
          '12月'
        ]
      })
      moment.updateLocale('ja', {
        weekdays: [
          '星期天',
          '星期一',
          '星期二',
          '星期三',
          '星期四',
          '星期五',
          '星期六'
        ]
      })
      break
    }
  }

  menu = () => {
    let { lang } = this.props.locale
    // let selectOptionList = configRenderLanguage.filter(
    //   obj => obj.lang !== lang
    // )
    const selectedIndex = configRenderLanguage.find(obj => obj.lang === lang)

    return (
      <React.Fragment>
        <Menu
          theme='light'
          mode='vertical'
          style={{ lineHeight: '36px',
            background: '#001529',
            color: '#fff',
            borderRadius: '12px',
            boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.1)'
          }}
          className='select-language-container'
          selectedKeys={[selectedIndex.lang]}
        >
          {configRenderLanguage.map((item, i) => (
            <Menu.Item key={item.lang} className='menu-flag-item' onClick={this.onSetLocale(item.lang)}>
              <span className={'disable'}>
                <img className='flags' alt={item.title} src={item.src} />
                {/* {`${item.title}`} */}
              </span>
            </Menu.Item>
          ))}
        </Menu>
      </React.Fragment>
    )
  };

  render () {
    const { fix } = this.props
    const { lang } = this.props.locale
    const currentSelectedObj = configRenderLanguage.find(
      obj => obj.lang === lang
    )
    return (
      <React.Fragment>
        <Dropdown overlayStyle={fix ? { position: 'fixed' } : {}} overlay={this.menu} trigger={['click']}>
          <a className='ant-dropdown-link'>
            <img
              className='selected-flag'
              alt={currentSelectedObj.title}
              src={currentSelectedObj.src}
            />
          </a>
        </Dropdown>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  locale: state.locale
})
const mapDispatchToProps = dispatch => {
  return {
    setLocale: bindActionCreators(StorageAction.setLocale, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectLanguage)

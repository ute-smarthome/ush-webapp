import React from 'react'

const IntervalSelector = (props) => {
  const { list, activeKey, onChange } = props
  const handleChangeValue = (key) => () => {
    onChange(key)
  }
  return (
    <ul className='interval-list'>
      {list.map((item) => {
        return (
          <li key={item.value}
            onClick={handleChangeValue(item.value)}
            className={activeKey === item.value && 'active'}>
            {item.title}
          </li>
        )
      })}
    </ul>
  )
}

export default IntervalSelector

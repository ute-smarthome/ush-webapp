import React from 'react'
import { convertDateMoment } from 'common/function'
import { connect } from 'react-redux'
import { get } from 'lodash'
const HistoryItem = props => {
  const { messages } = props.locale
  const { item } = props
  return (
    <li className='history-item'>
      <div
        className={`history-item__type history-item__type--${
          get(item, 'isLong') === true ? 'long' : 'short'
        }`}
      >
        {get(item, 'isLong') === true ? 'L' : 'S'}
      </div>
      <div className='flex justify-space-between'>
        <div className='history-item__symbol'>TRX/ ETH</div>
        <div className='history-item__create-date'>
          {convertDateMoment(new Date())}
        </div>
      </div>
      <div className='flex justify-space-between'>
        <div className='history-item__label'>{messages.price}</div>
        <div className='history-item__price'>
          <b>0.0283</b> ETH
        </div>
      </div>
      <div className='flex justify-space-between'>
        <div className='history-item__label'>{messages.amount}</div>
        <div className='history-item__amount'>
          <b>2</b> TRX
        </div>
      </div>
      <div className='flex justify-space-between'>
        <div className='history-item__label'>{messages.fee}</div>
        <div>0.0000011 ETH</div>
      </div>
      <div className='flex justify-space-between'>
        <div className='history-item__label'>{messages.total}</div>
        <div>0.23001181 ETH</div>
      </div>
      <div className='history-item__divider' />
      <div className='flex justify-space-between'>
        <div>
          <div className='history-item__label'>{messages.openPrice}</div>
          <div className='history-item__open-price'>5941.66</div>
          <div className='history-item__label'>{messages.closingPrice}</div>
          <div className='history-item__close-price'>5941.76</div>
        </div>
        <div>
          <div className='history-item__label'>{messages.openTime}</div>
          <div className='history-item__price-time'>
            {convertDateMoment(new Date())}
          </div>
          <div className='history-item__label'>{messages.closingTime}</div>
          <div className='history-item__price-time'>
            {convertDateMoment(new Date())}
          </div>
        </div>
      </div>
    </li>
  )
}

const mapStateToProps = (state) => ({
  userRedux: state.userRedux,
  locale: state.locale
})

export default connect(mapStateToProps)(HistoryItem)

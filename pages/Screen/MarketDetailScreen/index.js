import React from 'react'
import { connect } from 'react-redux'
import Breadcrumb from 'pages/Components/Breadcrumb'
import { Row, Col, Form, Input, Select, Modal } from 'antd'
import { get } from 'lodash'

import './style.scss'
import { images } from 'config/images'
import { convertDateMoment, splitPerp, formatNumberBro } from 'common/function'
import HistoryItem from './Components/HistoryItem'
import Favorite from 'pages/Components/Favorite'
import BaseAPI from 'controller/API/BaseAPI'
import { CaretDownOutlined } from '@ant-design/icons'
import IntervalSelector from './Components/IntervalSelector'
import MyButton from 'pages/Components/MyButton'
class MarketDetailScreen extends React.PureComponent {
  static getInitialProps = ({ query }) => {
    return { query }
  };
  constructor (props) {
    super(props)

    this.state = {
      marketList: [],
      historyList: [
        {
          id: 1,
          isLong: false
        },
        {
          id: 2,
          isLong: true
        }
      ],
      interval: '1m',
      isShowModal: false,
      txtBalance: 0,
      isLoadingFirst: true,
      selectedBase: props.query.id
    }
  }
  async componentDidMount () {
    const { userRedux } = this.props

    const response = await BaseAPI.getData('markets')

    if (userRedux) {
      const txtBalance = await BaseAPI.getData('wallet/balances')
      this.setState({ txtBalance })
    }

    if (response) this.setState({ marketList: response })
  }

  handleChangeInterval = (interval) => {
    this.setState({ interval })
  };

  handleToggleModal = (isShowModal) => () => {
    this.setState({ isShowModal })
  };

  handleChangeBase = (selectedBase) => {
    this.setState({ selectedBase })
  }

  renderHistoryList = () => {
    const { historyList } = this.state
    return historyList.map((item) => (
      <HistoryItem key={get(item, 'id')} item={item} />
    ))
  };

  render () {
    const { messages } = this.props.locale
    const { userRedux, tickerRedux } = this.props
    const { marketList, interval, selectedBase, txtBalance, isShowModal } = this.state
    const findItem = tickerRedux.find(item => item.symbol.includes(selectedBase))
    const ask = parseFloat(get(findItem, 'ask', 0))
    const bid = parseFloat(get(findItem, 'bid', 0))
    const path = [
      {
        path: '/',
        title: 'Trang Chủ'
      },
      {
        path: '/market',
        title: messages.market
      },
      {
        path: '/market/btc-utdt',
        title: 'BTC-USDT'
      }
    ]

    const intervalList = [
      {
        value: '1m',
        title: '1 M'
      },
      {
        value: '5m',
        title: '5 M'
      },
      {
        value: '10m',
        title: '10 M'
      },
      {
        value: '15m',
        title: '15 M'
      },
      {
        value: '24h',
        title: '1 D'
      }
    ]

    return (
      <div className='market-detail-container'>
        <Breadcrumb path={path} />
        <Row gutter={[16, 16]} className='market-container'>
          <Col xs={24} lg={12} xxl={11}>
            <div className='flex align-center market'>
              <div className='market__favorite'>
                <Favorite onChange={() => alert('test')} />
              </div>
              <div className='market__coin-icon'>
                <img
                  src={`https://raw.githubusercontent.com/nullablebool/crypto-icons/f199603c/200x200/${selectedBase}-200.png`}
                  alt=''
                />
              </div>
              <div className='market__coins'>
                <Select
                  defaultValue={selectedBase}
                  onChange={this.handleChangeBase}
                  suffixIcon={
                    <CaretDownOutlined style={{ color: '#ffc591' }} />
                  }
                >
                  {marketList.map((market) => {
                    return (
                      <Select.Option
                        key={get(market, 'base')}
                        value={get(market, 'base')}
                      >
                        {splitPerp(get(market, 'symbol', ''))}
                      </Select.Option>
                    )
                  })}
                </Select>
              </div>
              <div className='market__price'>
                {'$' + formatNumberBro(get(findItem, 'exPrice', 0))}
              </div>
              {/* <div className='market__change'>
                +$239 (<ins>2.08%</ins>)
                <img src={images.icTrend} alt='' />
              </div> */}
              {/* <div className='market__highlow'>
                <div className='market__highlow--high'>
                  <span>{messages.high}</span>
                  <span>8016.11</span>
                </div>
                <div className='market__highlow--low'>
                  <span>{messages.low}</span>
                  <span>8016.11</span>
                </div>
              </div> */}
            </div>
          </Col>
        </Row>
        <Row className='market-container' gutter={[16, 16]}>
          <Col xs={24} lg={24} xxl={11}>
            <div className='market-card'>
              <IntervalSelector
                onChange={this.handleChangeInterval}
                activeKey={interval}
                list={intervalList}
              />
              <iframe
                className='market__chart'
                src={`http://45.77.45.29:3050/?symbol=${selectedBase}-PERP&interval=${interval}`}
              />
            </div>
          </Col>
          <Col xs={24} lg={12} xxl={7}>
            <div className='market-card'>
              <Form>
                <Row gutter={[20, 20]}>
                  <Col xs={12}>
                    <Form.Item
                      label={messages.margin}
                      className='my-input my-input--purple my-input--hasAddonAfter'
                    >
                      <Input />
                      <div className='insertAfter'>USDT</div>
                    </Form.Item>
                  </Col>
                  <Col xs={12}>
                    <Form.Item
                      label={messages.limit}
                      className='my-input my-input--purple'
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col xs={12}>
                    <Form.Item className='my-input my-input--blue my-input--hasAddonBefore'>
                      <Input placeholder={messages.opt} />
                      <div className='insertBefore'>{messages.tp}</div>
                    </Form.Item>
                  </Col>
                  <Col xs={12}>
                    <Form.Item className='my-input my-input--blue my-input--hasAddonBefore'>
                      <Input placeholder={messages.opt} />
                      <div className='insertBefore'>{messages.sl}</div>
                    </Form.Item>
                  </Col>
                  <Col xs={10}>
                    <img
                      src={images.icSetting}
                      className='market-card__icon'
                      alt=''
                      onClick={this.handleToggleModal(true)}
                    />
                  </Col>
                  <Col xs={14}>
                    <div className='market-card__balance'>
                      {messages.availableBalance} :
                      <span className='text text--pink'>{` ${txtBalance} USDT`}</span>
                    </div>
                  </Col>
                  <Col xs={12}>
                    <button className='btn-custom btn-custom--long'>
                      <span>{messages.long}</span>
                      <small>{ask.toFixed(4)}</small>
                    </button>
                  </Col>
                  <Col xs={12}>
                    <button className='btn-custom btn-custom--short'>
                      <span>{messages.short}</span>
                      <small>{bid.toFixed(4)}</small>
                    </button>
                  </Col>
                </Row>
              </Form>
            </div>
          </Col>
          <Col xs={24} lg={12} xxl={6}>
            <div className='market-card'>
              <h2 className='market-card__title'>{messages.historyTransaction}</h2>
              {/* History list Here  */}
              <ul className='history-list'>{this.renderHistoryList()}</ul>
            </div>
          </Col>
        </Row>

        <Modal
          visible={isShowModal}
          onCancel={this.handleToggleModal(false)}
          onOk={this.handleToggleModal(false)}
          footer={null}
          className='modal-container'
          width='60%'
        >
          <div className='popup-icon'>
            <img src={images.popupCoverIcon} alt='' />
          </div>
          <div className='cover'>
            <img src={images.popupCover} alt='' />
          </div>
          <h4>{messages.contractOrderSLAndTP}</h4>
          <div className='flex justify-space-between' style={{ width: '100%' }}>
            <div>
              {messages.defaultTPRatio} (<span>{'<=1000'}</span>%)
              <div className='flex'>
                <button className='btn-custom'>
                  <img src={images.icMinus} alt='' />
                </button>
                <Form.Item className='my-input my-input--purple flex'>
                  <Input />
                </Form.Item>
                <button className='btn-custom'>
                  <img src={images.icPlus} alt='' />
                </button>
              </div>
            </div>
            <div>
              {messages.defaultSLRatio} (<span>{'<=80'}</span>%)
              <div className='flex'>
                <button className='btn-custom'>
                  <img src={images.icMinus} alt='' />
                </button>
                <Form.Item className='my-input my-input--purple flex'>
                  <Input />
                </Form.Item>
                <button className='btn-custom'>
                  <img src={images.icPlus} alt='' />
                </button>
              </div>
            </div>
            <div>
              {messages.defaultOrderMargin} (USDT)
              <div className='flex'>
                <button className='btn-custom'>
                  <img src={images.icMinus} alt='' />
                </button>
                <Form.Item className='my-input my-input--purple flex'>
                  <Input />
                </Form.Item>
                <button className='btn-custom'>
                  <img src={images.icPlus} alt='' />
                </button>
              </div>
            </div>
          </div>
          <div className='text text-right'>
            <MyButton title={messages.cancel} transparent />
            <MyButton title={messages.ok} />
          </div>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  tickerRedux: state.tickerRedux,
  userRedux: state.userRedux,
  locale: state.locale
})

export default connect(mapStateToProps)(MarketDetailScreen)

import React from 'react'
import MyButton from 'pages/Components/MyButton'
import { Form, Input, Select, Modal } from 'antd'
import { showNotification, getLength } from 'common/function'
import { typeOutputDevice, typeDeviceAddNew } from 'common/constants'
import BaseAPI from 'controller/API/BaseAPI'
import ReduxServices from 'common/redux'
import { connect } from 'react-redux'
import { get } from 'lodash'
const { Option } = Select

const InitialState = {
  txtTypeOutput: null,
  visible: false
}

class SettingComponent extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = InitialState
  }

   onChangeSginalType= (name) => (value) => {
     this.setState({
       [name]: value
     })
   }

   onResetState = (data) => {
     const keys = Object.keys(this.state)
     const stateReset = keys.reduce((acc, v) => ({ ...acc, [v]: undefined }), {})
     this.setState({ ...stateReset, ...InitialState, ...data })
   }

  onChangeArea = (index) => (value) => {
    const { areaList } = this.props.deviceRedux
    const itemArea = areaList.find(item => item.id === value)
    this.setState({
      [`txtArea${index}`]: itemArea.id,
      [`txtAreaName${index}`]: itemArea.name

    })
  }

  checkUndefined = (type) => {
    const check = typeOutputDevice[type].value.map((item, index) => {
      if (!getLength(get(this.state, `txtAreaName${index + 1}`)) > 0) return false
      if (!getLength(get(this.state, `txtArea${index + 1}`)) > 0) return false
      if (!item && !getLength(get(this.state, `txtSignal${index + 1}`)) > 0) return false
      return true
    })

    return !check.includes(false)
  }

  children = (index) => {
    const { deviceRedux } = this.props
    const { areaList } = deviceRedux
    return areaList && areaList.map(item => {
      return (
        <>
          <Option
            value={item.id}
            disabled={get(this.state, `txtArea${index}`) === item.id}
          >
            {item.name}
          </Option>
        </>
      )
    })
  }

  childrenSignal = (idx) => {
    const arrKey = Object.keys(typeDeviceAddNew)
    const result = arrKey.map((item, index) => {
      return (
        <Option key={index} value={item} disabled={get(this.state, `txtSignal${idx}`) === item}>
          {typeDeviceAddNew[item].name}
        </Option>
      )
    })
    return result
  }

  childrenOutput = () => {
    const { txtTypeOutput } = this.state
    const arrKey = Object.keys(typeOutputDevice)
    const result = arrKey.map((item, index) => {
      return (
        <Option key={index} value={item} disabled={txtTypeOutput === item}>
          {typeOutputDevice[item].name}
        </Option>
      )
    })
    return result
  }

  onChangeValue = () => (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  onAddDevice = () => async () => {
    // const { txtAreaName, txtAreaId, txtDeviceName, txtTypeOutput } = this.state
    // console.log('SettingComponent -> onAddDevice ->  txtAreaName, txtAreaId, txtDeviceName, txtTypeOutput', txtAreaName, txtAreaId, txtDeviceName, txtTypeOutput)
    const { txtTypeOutput } = this.state
    if (getLength(txtTypeOutput) > 0 && this.checkUndefined(txtTypeOutput)) {
      const payload = typeOutputDevice[txtTypeOutput].value.map((item, index) => {
        return {
          name: get(this.state, `txtDeviceName${index + 1}`),
          signalTypeId: item ? 1 : get(typeDeviceAddNew, `${get(this.state, `txtSignal${index + 1}`)}.value`),
          areaName: get(this.state, `txtAreaName${index + 1}`),
          areaId: get(this.state, `txtArea${index + 1}`)
        }
      })
      const resDevice = await BaseAPI.postData('device', { data: payload })
      if (resDevice) {
        showNotification('Thêm thiết bị thành công !')
        this.onResetState({
          txtKey: resDevice[0].key,
          visible: true
        })
      } else {
        showNotification('Đã có lỗi xảy ra !')
      }
    } else {
      showNotification('Chọn loại thiết bị rỗng !')
    }
  }

  onChangeTypeOutput = () => (value) => {
    this.onResetState({ txtTypeOutput: value })
  }
  handleCancel =() => () => {
    this.setState({
      visible: false
    })
  }
  onCopyKey = () => () => {
    const { txtKey } = this.state
    if (getLength(txtKey) > 0) {
      navigator.clipboard.writeText(txtKey).then(() => {
        showNotification('Đã sao chép key !')
      })
    } else {
      showNotification('Key rỗng !')
    }
  }

  onRenderDevice = () => {
    const { txtTypeOutput } = this.state
    let result = []
    if (txtTypeOutput) {
      result = typeOutputDevice[txtTypeOutput].value.map((item, index) => {
        return (
          <>
            <div style={{ marginBottom: '0.5rem' }}>
              <label >{`Cài đặt thiết bị ${index + 1}`}</label>
            </div>
            <Form key={index} style={{ borderStyle: 'solid', borderRadius: '1rem', borderColor: 'red', marginBottom: '1rem', padding: '1rem' }}>
              <Form.Item
                label={`Tên thiết bị`}
                className='my-input my-input--purple'
              >
                <Input
                  onChange={this.onChangeValue()}
                  placeholder='Nhập tên '
                  name={`txtDeviceName${index + 1}`}
                  value={get(this.state, `txtDeviceName${index + 1}`)}

                />
              </Form.Item>
              <div style={{ marginBottom: '0.5rem' }}>
                <label >{`Tên khu vực`}</label>
              </div>
              <Select
                className='my-input'
                placeholder='Chọn khu vực'
                style={{ borderRadius: '12px', marginBottom: '24px' }}
                title={'hahahaha'}
                onChange={this.onChangeArea(index + 1)}
                value={get(this.state, `txtArea${index + 1}`)}
              >
                {this.children(index + 1)}
              </Select>
              {!item ? (
                <>
                  <div style={{ marginBottom: '0.5rem' }}>
                    <label >{`Loại thiết bị`}</label>
                  </div>
                  <Select
                    className='my-input'
                    placeholder='Chọn'
                    style={{ borderRadius: '12px', marginBottom: '24px' }}
                    onChange={this.onChangeSginalType(`txtSignal${index + 1}`)}
                    value={get(this.state, `txtSignal${index + 1}`)}
                  >
                    {this.childrenSignal(index + 1)}
                  </Select>
                </>
              ) : ''}
            </Form>
          </>
        )
      })
    }
    return result
  }

  render () {
    const { txtAreaName, visible, txtKey } = this.state
    return (
      <>
        <div className='align-center'>
          <div style={{ marginBottom: '0.5rem' }}>
            <label >{'Đầu ra của thiết bị:'}</label>
          </div>
          <Select
            className='my-input'
            placeholder='Chọn'
            style={{ borderRadius: '12px', marginBottom: '24px' }}
            onChange={this.onChangeTypeOutput()}
          >
            {this.childrenOutput()}
          </Select>
          {this.onRenderDevice()}
          <MyButton onClick={this.onAddDevice()} className='ref-card__btn btn'>Thêm mới</MyButton>
        </div>
        <Modal
          title='Token thiết bị'
          visible={visible}
          onCancel={this.handleCancel()}
          footer={null}
        >
          <div className='align-center'>
            <Form.Item
              label=''
              className='my-input my-input--purple'

            >
              <Input placeholder='Nhập tên người dùng' value={txtKey} disabled />
            </Form.Item>
            <div className='button-container'>
              <MyButton onClick={this.onCopyKey()} className='ref-card__btn'>Sao chép</MyButton>
            </div>
          </div>
        </Modal>

      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    deviceRedux: state.deviceRedux
  }
}

export default connect(mapStateToProps)(SettingComponent)

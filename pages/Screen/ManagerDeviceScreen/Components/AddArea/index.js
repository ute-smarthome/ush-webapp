import React from 'react'
import { get } from 'lodash'
import { showNotification } from 'common/function'
import { faChartArea } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import MyButton from 'pages/Components/MyButton'
import ReduxServices from 'common/redux'
import BaseAPI from 'controller/API/BaseAPI'
import { connect } from 'react-redux'

class AddArea extends React.PureComponent {
  onDelete = (item) => async () => {
    const payload = await BaseAPI.deleteData('area', { id: item.id, status: 'INACTIVE' })
    console.log('AddArea -> onDelete -> payload', payload)
    if (payload) {
      ReduxServices.deleteAreaDeviceRedux(payload)
      showNotification(get(payload, 'message'))
    } else {
      showNotification(get(payload, 'message') || 'Đã có lỗi xảy ra!')
    }
  }
  render () {
    const { item, deviceRedux } = this.props
    const total = deviceRedux && deviceRedux.data.filter(itm => itm.areaId === item.id).length
    return (
      <li className='transaction-list-item'>
        <div className={`transaction-list-item__type transaction-list-item__type--true}`}>
          <FontAwesomeIcon icon={faChartArea} style={{ maxWidth: '100%', fontSize: '24px', textAlign: 'center', color: 'red' }} />
        </div>
        <div className='transaction-list-item__total '>
              Tên khu vực
          <div className='transaction-list-item__total--down'>{item.name}</div>
        </div>

        <div className='transaction-list-item__total '>
              Số lượng thiết bị
          <div className='transaction-list-item__total--down'>{total || 0}</div>
        </div>

        <div className='transaction-list-item__total '>
          <div>Hành động</div>
          <MyButton onClick={this.onDelete(item)} className='transaction-list-item__btn'>Xóa</MyButton>
        </div>
      </li>
    )
  }
}
const mapStateToProps = state => {
  return {
    deviceRedux: state.deviceRedux
  }
}
export default connect(mapStateToProps)(AddArea)

import React from 'react'
import { get } from 'lodash'
import { faLightbulb, faFan } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ManaDevice extends React.PureComponent {
  render () {
    const { item } = this.props
    console.log('AddArea -> render -> item', item)
    return (
      <li className='transaction-list-item'>
        <div className={`transaction-list-item__type transaction-list-item__type--true}`}>
          <FontAwesomeIcon icon={get(item, 'signalTypeId') === '2' ? faFan : faLightbulb} style={{ maxWidth: '100%', fontSize: '24px', textAlign: 'center', color: 'red' }} />
        </div>
        <div className='transaction-list-item__total '>
            Tên thiết bị
          <div className='transaction-list-item__total--down'>{item.name}</div>
        </div>
        <div className='transaction-list-item__total '>
            Tên khu vực
          <div className='transaction-list-item__total--down'>{item.areaName}</div>
        </div>
      </li>
    )
  }
}

export default ManaDevice

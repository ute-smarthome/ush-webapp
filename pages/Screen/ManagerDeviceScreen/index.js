import React from 'react'
import { connect } from 'react-redux'
import Breadcrumb from 'pages/Components/Breadcrumb'
import { Row, Col, Form, Input, Modal } from 'antd'
import './style.scss'
import { withRouter } from 'next/router'
import ReduxServices from 'common/redux'
import AddDevice from './Components/AddDevice'
import AddArea from './Components/AddArea'
import ManaDevice from './Components/ManaDevice'
import MyButton from 'pages/Components/MyButton'
import { getLength, showNotification } from 'common/function'
import BaseAPI from 'controller/API/BaseAPI'
import { has, get } from 'lodash'

class SettingScreen extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      txtKey: ''
    }
  }

  renderAddDevice = () => {
    this.onRendeManaDevice()
    return <AddDevice />
  }

  renderAreaList = () => {
    const { areaList } = this.props.deviceRedux
    const result = areaList && areaList.map((item, index) => <AddArea item={item} key={index} />)
    return result
  }

  onChangeValue = () => (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  onResetValueChange = () => {
    this.setState({
      txtAddArea: ''
    })
  }

  onRederItemDevice = (data) => {
    const result = data.map((item, index) => {
      return <ManaDevice item={item} key={index} />
    })
    return result
  }

  onDeleteDevice = (item) => async () => {
    if (getLength(item) > 0) {
      const payload = await BaseAPI.deleteData('device', { id: item, status: 'INACTIVE' })
      console.log('SettingScreen -> onDeleteDevice -> payload', payload)
      if (payload && get(payload, 'ok') > 0) {
        ReduxServices.deleteDeviceRedux({ id: item })
        showNotification('Xóa thiết bị thành công !')
      } else {
        showNotification('Đã có lỗi xảy ra !')
      }
    } else {
      showNotification('Đã có lỗi xảy ra !')
    }
  }

  onRendeManaDevice = () => {
    const { deviceRedux } = this.props
    const { data } = deviceRedux
    let objDevice = {}
    data && data.forEach((item) => {
      if (!has(objDevice, `${item.keyId}`)) {
        objDevice[`${item.keyId}`] = []
        objDevice[`${item.keyId}`].push(item)
      } else {
        objDevice[`${item.keyId}`].push(item)
      }
    })
    const keys = Object.keys(objDevice)
    const result = keys && keys.map((item, index) => {
      return (
        <Form
          key={index}
          style={{ borderStyle: 'solid', borderRadius: '1rem', borderColor: 'red', marginBottom: '1rem', padding: '1rem' }}
        >
          <ul className='transaction-list'>
            {
              this.onRederItemDevice(objDevice[item])
            }
          </ul>
          <MyButton onClick={this.onDeleteDevice(item)} className='ref-card__btn'>Xóa</MyButton>
        </Form>
      )
    })
    return result
  }

  addAea = () => async () => {
    const { txtAddArea } = this.state
    if (getLength(txtAddArea) > 0) {
      const payload = await BaseAPI.postData('area', { name: txtAddArea })
      if (payload) {
        ReduxServices.addNewAreaDeviceRedux(payload)
        this.onResetValueChange()
        showNotification('Thêm khu vực thành công !')
      } else {
        showNotification('Đã có lỗi xảy ra !')
      }
    } else {
      showNotification('Trường nhập rỗng !')
    }
  }

  render () {
    const { txtAddArea, txtKey } = this.state
    const { messages } = this.props.locale
    const path = [
      {
        path: '/manager',
        title: 'Quản lý'
      }
    ]
    return (
      <div className='wallet-container'>
        <Breadcrumb path={path} />

        <Row >
          <Col xs={24} md={12} xl={8} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                 Thêm thiết bị
              </h4>
              {this.renderAddDevice()}
            </div>
          </Col>
          <Col xs={24} md={12} xl={8} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Quản lý thiết bị
              </h4>
              {this.onRendeManaDevice()}
            </div>
          </Col>
          <Col xs={24} md={12} xl={8} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Thêm khu vực
              </h4>
              <ul className='transaction-list'>
                {this.renderAreaList()}
              </ul>
              <div className='flex justify-space-between align-center'>
                <Form.Item
                  label='Thêm người dùng'
                  className='my-input my-input--purple'
                >
                  <Input name={'txtAddArea'} value={txtAddArea} onChange={this.onChangeValue()} placeholder='nhập tên khu vực' />
                </Form.Item>
                <MyButton onClick={this.addAea()} className='ref-card__btn'>Thêm</MyButton>
              </div>
            </div>
          </Col>

        </Row>

      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userRedux: state.userRedux,
  tickerRedux: state.tickerRedux,
  deviceRedux: state.deviceRedux,
  locale: state.locale

})

export default connect(mapStateToProps)(withRouter(SettingScreen))

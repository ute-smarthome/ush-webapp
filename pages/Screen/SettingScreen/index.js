import React from 'react'
import { connect } from 'react-redux'
import Breadcrumb from 'pages/Components/Breadcrumb'
import { Row, Col, Form, Input, TimePicker, Select, Checkbox } from 'antd'
import './style.scss'
import HistoryListItem from './Components/HistoryListItem'
import ShareList from './Components/ShareComponent'
import SettingComponent from './Components/SettingComponent'
import SettingAreaComponent from './Components/SettingAreaComponent'
import BaseAPI from 'controller/API/BaseAPI'
import MyButton from 'pages/Components/MyButton'
import { showNotification, getLength } from 'common/function'
import { typeDay, defaultCronPayload, typeTimer } from 'common/constants'
import { get, isEmpty } from 'lodash'
import { withRouter } from 'next/router'
import ReduxServices from 'common/redux'
import moment from 'moment'
import { findDOMNode } from 'react-dom'
const { Option } = Select

class SettingScreen extends React.PureComponent {
  static getInitialProps ({ query }) {
    return { query }
  }
  constructor (props) {
    super(props)
    this.state = {
      arrListHistory: [],
      device: '',
      txtAdduser: '',
      txtTypeDevice: [],
      txtTime: '00:00',
      arrTimeList: []
    }

    this.modal = React.createRef()
  }

  async componentDidMount () {
    const { query } = this.props
    const { id } = query
    const arrListHistory = await BaseAPI.getData(`history/item/${id}`)
    const arrTimeList = await BaseAPI.getData('time')
    console.log('componentDidMount -> arrTimeList', arrTimeList)
    if (arrListHistory && arrTimeList) {
      this.setState({
        arrListHistory,
        arrTimeList
      })
    }
  }

  componentDidUpdate () {
    const { deviceRedux } = this.props
    if (!isEmpty(deviceRedux)) {
      this.loadDeviceItem()
    }
  }

  onResetValueChange = () => {
    this.setState({
      txtAdduser: '',
      txtTypeDevice: [],
      txtTypeTimer: [],
      txtTime: '00:00'
    })
  }

  updateRes = (payload) => {
    const { arrTimeList } = this.state
    const newTimeList = arrTimeList.slice()
    const findIndex = arrTimeList.findIndex(item => item._id === payload._id)
    if (findIndex !== -1) {
      newTimeList.spice(findIndex, 1, payload)
      this.setState({ arrTimeList: newTimeList })
    } else {
      newTimeList.push(payload)
      this.setState({ arrTimeList: newTimeList })
    }
  }

  loadDeviceItem = () => {
    const { query, deviceRedux } = this.props
    const { id } = query
    const device = deviceRedux && deviceRedux.data.find(item => item.id === id)
    this.setState({
      device
    })
  }

  onWithdraw = () => {
    this.modal.current.openModal('Withdraw')
  }

  onDeposit = () => {
    this.modal.current.openModal('Deposit')
  }

  renderHistoryDevice = () => {
    const { arrListHistory } = this.state
    return arrListHistory && arrListHistory.slice(0, 5).map(item => <HistoryListItem key={item.id} item={item} />)
  }

  renderHistoryTime = () => {
    return true
  }

  renderShareList = () => {
    const { device } = this.state
    const { userId } = device
    return userId && userId.map((item, index) => <ShareList key={index} item={item} deviceId={device.id} />)
  }
  renderSetting = () => {
    const { device } = this.state

    return device && <SettingComponent key={device._id} item={device} />
  }
  renderSettingArea = () => {
    const { device } = this.state

    return device && <SettingAreaComponent key={device._id} item={device} />
  }
  addUser = () => async () => {
    const { txtAdduser, device } = this.state
    if (getLength(txtAdduser) > 0) {
      const payload = await BaseAPI.putData('device/share', { deviceId: device.id, userShareId: txtAdduser, type: true })
      if (get(payload, 'success')) {
        ReduxServices.updateShareDeviceRedux(get(payload, 'data'))
        this.onResetValueChange()
        showNotification(get(payload, 'message'))
      } else {
        showNotification(get(payload, 'message') || 'Đã có lỗi xảy ra!')
      }
    } else {
      showNotification('Thẻ nhập không được trống !')
    }
  }
  onChangeValue = () => (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  children = () => {
    const arrDay = Object.keys(typeDay)
    return arrDay && arrDay.map(item => {
      return (
        <>
          <Option
            value={item}
          >
            {typeDay[item]}
          </Option>
        </>
      )
    })
  }
  onChangeStatus = () => (value) => {
    if (getLength(value) > 0) {
      if (getLength(value) === 1) {
        this.setState({
          txtTypeDevice: value
        })
      } else {
        const preValue = this.state.txtTypeDevice[0]
        const findIndex = value.findIndex(item => item === preValue)
        value.splice(findIndex, 1)
        this.setState({
          txtTypeDevice: value
        })
      }
    } else {
      this.setState({
        txtTypeDevice: []
      })
    }
  }

  onChangeType = () => (value) => {
    if (getLength(value) > 0) {
      if (getLength(value) === 1) {
        this.setState({
          txtTypeTimer: value
        })
      } else {
        const preValue = this.state.txtTypeTimer[0]
        const findIndex = value.findIndex(item => item === preValue)
        value.splice(findIndex, 1)
        this.setState({
          txtTypeTimer: value
        })
      }
    } else {
      this.setState({
        txtTypeTimer: []
      })
    }
  }

  onChangeTime = (name) => (momentTime, stringTime) => {
    this.setState({
      [name]: stringTime
    })
  }

  onChangeValueDay = (name) => (value, option) => {
    this.setState({
      [name]: value
    })
  }
  onSubmitTime = () => async () => {
    let { txtTime, arrDay, txtTypeDevice, txtTypeTimer } = this.state
    const { device } = this.state
    txtTypeDevice = getLength(txtTypeDevice) > 0 ? txtTypeDevice : ['Off']
    if (getLength(txtTime) > 0) {
      const arrTime = txtTime.split(':').reverse().map(item => [parseInt(item)])
      arrTime.push(defaultCronPayload.defaultDate)
      arrTime.push(defaultCronPayload.defaultMonth)
      if (arrDay && getLength(arrDay) > 0) {
        arrTime.push(arrDay.map(Number))
      } else {
        arrTime.push(defaultCronPayload.defaultDay)
      }
      const action = {
        key: device.key,
        data: {
          type: 'controllDevice',
          deviceId: device.id,
          value: 0,
          status: txtTypeDevice[0] === 'On'
        }
      }
      const type = txtTypeTimer[0] === typeTimer.loop.name ? typeTimer.loop.value : typeTimer.oneTime.value
      const payload = await BaseAPI.postData('time', { timeExpression: arrTime, type, action })
      if (payload) {
        console.log('onSubmitTime -> payload', payload)
        this.onResetValueChange()
        showNotification('Đã đặt giờ thành công !')
        // update list
        this.updateRes(payload)
      }
    } else {
      showNotification('Trường thời gian không được để trống !')
    }
  }

  render () {
    const { messages } = this.props.locale
    const { txtAdduser, txtTypeDevice, txtTypeTimer, txtTime } = this.state
    const path = [
      {
        path: '/',
        title: 'Trang Chủ'
      },
      {
        path: '/setting',
        title: 'Cài đặt'
      }
    ]
    return (
      <div className='wallet-container'>
        <Breadcrumb path={path} />

        <Row >
          <Col xs={24} md={12} xl={{ offset: 2, span: 8 }} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Cài đặt thiết bị
              </h4>
              {this.renderSetting()}
            </div>
          </Col>

          <Col xs={24} md={12} xl={{ offset: 2, span: 8 }} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Cài đặt khu vực
              </h4>
              {this.renderSettingArea()}
            </div>
          </Col>

        </Row>
        <Row>

          <Col xs={24} md={12} xl={8} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Chia sẻ thiết bị
              </h4>
              <ul className='transaction-list'>
                {this.renderShareList()}
              </ul>
              <div className='flex justify-space-between align-center'>
                <Form.Item
                  label='Thêm người dùng'
                  className='my-input my-input--purple'
                >
                  <Input name={'txtAdduser'} value={txtAdduser} onChange={this.onChangeValue()} placeholder='Nhập tên tài khoản' />
                </Form.Item>
                <MyButton onClick={this.addUser()} className='ref-card__btn'>Thêm</MyButton>
              </div>
            </div>
          </Col>
          <Col xs={24} md={12} xl={8} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Hẹn giờ
              </h4>
              <Form style={{ borderStyle: 'solid', borderRadius: '1rem', borderColor: 'red', marginBottom: '1rem', padding: '1rem' }}>
                <Form.Item
                  label='Thời gian'
                  className=' my-input--purple'
                >
                  <TimePicker
                    allowClear={false}
                    format={'HH:mm'}
                    defaultValue={moment('00:00', 'HH:mm')}
                    onChange={this.onChangeTime('txtTime')}
                    value={moment(txtTime, 'HH:mm')}
                  />
                </Form.Item>
                <Form.Item
                  style={{ marginTop: '-2rem' }}
                  label='Trạng thái'
                  className=' my-input--purple'
                >
                  <Checkbox.Group
                    options={['On', 'Off']}
                    style={{ display: 'flex', justifyContent: 'space-evenly' }}
                    value={txtTypeDevice}
                    onChange={this.onChangeStatus()}
                  />
                </Form.Item>
                <Form.Item
                  style={{ marginTop: '-2rem' }}
                  label='Loại'
                  className='my-input--purple my-input--padingLeft'
                >
                  <Checkbox.Group
                    options={[typeTimer.loop.name, typeTimer.oneTime.name]}
                    style={{ display: 'flex', justifyContent: 'space-evenly' }}
                    value={txtTypeTimer}
                    onChange={this.onChangeType()}
                  />
                </Form.Item>
                {
                  getLength(txtTypeTimer) > 0 && txtTypeTimer[0] === typeTimer.loop.name ? (
                    <Form.Item
                      label='Day'
                      className='my-input my-input--purple my-input--ant-spec-item'
                    >
                      <Select
                        mode='multiple'
                        className='my-input'
                        placeholder='Select a person'
                        style={{ borderRadius: '12px', marginBottom: '24px' }}
                        onChange={this.onChangeValueDay('arrDay')}
                      >
                        {this.children()}
                      </Select>
                    </Form.Item>

                  ) : ''
                }
                <MyButton
                  onClick={this.onSubmitTime()}
                  className='ref-card__btn'
                >Xác nhận</MyButton>
              </Form>
              <ul className='transaction-list'>
                {this.renderHistoryTime()}
              </ul>
            </div>
          </Col>
          <Col xs={24} md={12} xl={8} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Lịch sử điều khiển
              </h4>
              <ul className='transaction-list'>
                {this.renderHistoryDevice()}
              </ul>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userRedux: state.userRedux,
  tickerRedux: state.tickerRedux,
  deviceRedux: state.deviceRedux,
  locale: state.locale

})

export default connect(mapStateToProps)(withRouter(SettingScreen))

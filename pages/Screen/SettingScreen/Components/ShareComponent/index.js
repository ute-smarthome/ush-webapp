import React from 'react'
import { get } from 'lodash'
import { showNotification } from 'common/function'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import MyButton from 'pages/Components/MyButton'
import ReduxServices from 'common/redux'
import BaseAPI from 'controller/API/BaseAPI'

class ShareList extends React.PureComponent {
  onDelete = (item, deviceId) => async () => {
    const payload = await BaseAPI.putData('device/share', { deviceId, userShareId: item, type: false })
    if (get(payload, 'success')) {
      ReduxServices.updateShareDeviceRedux(get(payload, 'data'))
      showNotification(get(payload, 'message'))
    } else {
      showNotification(get(payload, 'message') || 'Đã có lỗi xảy ra!')
    }
  }
  render () {
    const { item, deviceId } = this.props

    return (
      <li className='transaction-list-item'>
        <div className={`transaction-list-item__type transaction-list-item__type--true}`}>
          <FontAwesomeIcon icon={faUser} style={{ maxWidth: '100%', fontSize: '24px', textAlign: 'center', color: 'red' }} />
        </div>
        <div className='transaction-list-item__total '>
              Tên tài khoản
          <div className='transaction-list-item__total--down'>{item}</div>
        </div>

        <div className='transaction-list-item__total '>
          <div>Hành động</div>
          <MyButton onClick={this.onDelete(item, deviceId)} className='transaction-list-item__btn'>Xóa</MyButton>
        </div>
      </li>
    )
  }
}

export default ShareList

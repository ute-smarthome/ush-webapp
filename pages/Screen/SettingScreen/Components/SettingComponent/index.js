import React from 'react'
import MyButton from 'pages/Components/MyButton'
import { Form, Input } from 'antd'
import { showNotification } from 'common/function'
import BaseAPI from 'controller/API/BaseAPI'
import ReduxServices from 'common/redux'

class SettingComponent extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      txtName: '',
      txtAreaName: '',
      txtKey: '',
      visible: false
    }
  }
  componentDidMount () {
    const { item } = this.props
    const { name, areaName, key } = item
    this.setState({
      txtName: name,
      txtAreaName: areaName,
      txtKey: key
    })
  }
  onCopyKey = () => () => {
    const { txtKey } = this.state
    navigator.clipboard.writeText(txtKey).then(() => {
      showNotification('Đã sao chép key !')
    })
  }
  onResetKey = () => () => {
    this.setState({
      visible: true
    })
  }

  onChangeValue = () => (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }
  onUpdateName = () => async () => {
    const { txtName } = this.state
    const { item } = this.props
    const payload = await BaseAPI.putData('device', { id: item.id, name: txtName })
    if (payload) {
      ReduxServices.updateDeviceRedux(payload)
      showNotification('Cập nhập thành công !')
    } else {
      showNotification('Đã có lỗi xảy ra !')
    }
  }

  onUpdateArea = () => () => {
    console.log(true)
  }
  render () {
    const { txtName, txtKey } = this.state

    return (
      <>
        <div className='align-center'>
          <Form.Item
            label='Tên thiết bị'
            className='my-input my-input--purple'
          >
            <Input placeholder='Nhập tên' name={'txtName'} value={txtName} onChange={this.onChangeValue()} />
          </Form.Item>
          <MyButton onClick={this.onUpdateName()} className='ref-card__btn btn'>Cập nhập</MyButton>
        </div>

        <div className='align-center'>
          <Form.Item
            label='Token thiết bị'
            className='my-input my-input--purple'

          >
            <Input placeholder='Input username' value={txtKey} disabled />
          </Form.Item>
          <div className='button-container'>
            <MyButton onClick={this.onCopyKey()} className='ref-card__btn'>Sao chép</MyButton>
          </div>
        </div>
      </>
    )
  }
}

export default SettingComponent

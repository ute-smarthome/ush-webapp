import React from 'react'
import { get } from 'lodash'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import MyButton from 'pages/Components/MyButton'
import ReduxServices from 'common/redux'
import { Form, Input, Dropdown, Menu, Select } from 'antd'
import { showNotification, getLength } from 'common/function'
import { connect } from 'react-redux'
import BaseAPI from 'controller/API/BaseAPI'
const { Option } = Select

class SettingComponent extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      txtAreaName: '',
      txtAreaId: '',
      txtNewArea: ''
    }
  }

  componentWillMount () {
    const { item } = this.props
    const { areaName, areaId } = item
    this.setState({
      txtAreaName: areaName,
      txtAreaId: areaId
    })
  }

  onResetValueChange = () => {
    this.setState({
      txtNewArea: ''
    })
  }

  onChangeValue = () => (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }
  onUpdateArea = () => async () => {
    const { txtAreaId } = this.state
    const { deviceRedux } = this.props
    const { areaList } = deviceRedux
    const findAreaItem = areaList.find(item => item.id === txtAreaId)
    const { item } = this.props
    const payload = await BaseAPI.putData('device', { id: item.id, areaName: findAreaItem.name, areaId: findAreaItem.id })
    if (payload) {
      console.log('SettingComponent -> onUpdateArea -> payload', payload)
      ReduxServices.updateAreaDeviceRedux(payload)
      showNotification('Thay đổi khu vực thành công !')
    } else {
      showNotification('Đã có lỗi xảy ra !')
    }
  }

  onAddNewArea = () => async () => {
    const { txtNewArea } = this.state
    if (getLength(txtNewArea) > 0) {
      const payload = await BaseAPI.postData('area', { name: txtNewArea })
      if (payload) {
        ReduxServices.addNewAreaDeviceRedux(payload)
        this.onResetValueChange()
        showNotification('Thêm khu vực thành công !')
      } else {
        showNotification('Đã có lỗi xảy ra !')
      }
    } else {
      showNotification('Trường nhập rỗng !')
    }
  }

  onChangeArea = () => (value) => {
    this.setState({
      txtAreaId: value
    })
  }

  onChangeValueBase = () => (e) => {
    console.log(e)
  }

  children = () => {
    const { deviceRedux } = this.props
    const { areaList } = deviceRedux
    const { txtAreaId } = this.state
    return areaList && areaList.map(item => {
      return (
        <>
          <Option
            value={item.id}
            disabled={txtAreaId === item.id}
          >
            {item.name}
          </Option>
        </>
      )
    })
  }

  render () {
    const { txtAreaName, txtNewArea } = this.state
    console.log('SettingComponent -> render -> txtAreaName', txtAreaName)
    return (
      <>
        <div className='align-center'>
          <div style={{ marginBottom: '0.5rem' }}>
            <label >{'Tên khu vực:'}</label>
          </div>
          <Select
            className='my-input'
            placeholder='Chọn khu vực'
            style={{ borderRadius: '12px', marginBottom: '24px' }}
            onChange={this.onChangeArea()}
            defaultValue={txtAreaName}
          >
            {this.children()}
          </Select>

          <MyButton onClick={this.onUpdateArea()} className='ref-card__btn btn'>Cập nhập</MyButton>
        </div>
        <div className='align-center'>
          <Form.Item
            label='Khu vực'
            className='my-input my-input--purple'
          >
            <Input placeholder='Nhập mới khu vực' name={'txtNewArea'} value={txtNewArea} onChange={this.onChangeValue()} />
          </Form.Item>
          <MyButton onClick={this.onAddNewArea()} className='ref-card__btn'>Thêm mới</MyButton>
        </div>

      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    deviceRedux: state.deviceRedux
  }
}

export default connect(mapStateToProps)(SettingComponent)

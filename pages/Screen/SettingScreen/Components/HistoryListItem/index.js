import React from 'react'
import { get } from 'lodash'
import { convertDateMoment } from 'common/function'

const HistoryListItem = props => {
  const { item } = props
  return (
    <li className='transaction-list-item'>
      <div className={`transaction-list-item__type transaction-list-item__type--${get(item, 'status')}`}>
        {get(item, 'status') ? 'On' : 'Off'}
      </div>
      <div className='transaction-list-item__total '>
        Thời gian
        <div className='transaction-list-item__total--down'>{convertDateMoment(get(item, 'createdAt'))}</div>
      </div>
      {/* <div className='transaction-list-item__amount'>{typeNameDevice[get(item, 'signalTypeId')]} USDT</div> */}
      <div className='transaction-list-item__total '>
        Tài khoản
        <div className='transaction-list-item__total--down overflow-text'>{get(item, 'createdUser')}</div>
      </div>
      <div className='transaction-list-item__total '>
        giá trị
        <div className='transaction-list-item__total--down'>{get(item, 'value')}</div>
      </div>
      <div className='transaction-list-item__total '>
        Tên
        <div className='transaction-list-item__total--down'>{get(item, 'deviceName')}</div>
      </div>
      {/* <div className='transaction-list-item__total '>
        <div className='transaction-list-item__total--down'>{typeNameDevice[get(item, 'signalTypeId')]}</div>
          TYPE
      </div> */}
    </li>
  )
}

export default HistoryListItem


import React from 'react'
import Breadcrumb from 'pages/Components/Breadcrumb'
import { Row, Col, Form, Input } from 'antd'
import MyButton from 'pages/Components/MyButton'
import BaseAPI from 'controller/API/BaseAPI'
import { showNotification, getLength } from 'common/function'

import './style.scss'

class HelpAndSupport extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      txtEmail: '',
      txtTitle: '',
      txtContent: ''
    }
  }
  onChangeValue = () => (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  onSubmit = () => async () => {
    const { txtEmail, txtTitle, txtContent } = this.state
    if (getLength(txtTitle) > 0 && getLength(txtContent) > 0) {
      const payload = await BaseAPI.postData('contactUs', { email: txtEmail, title: txtTitle, content: txtContent })
      if (payload) {
        showNotification('Đã gửi thành công !')
        this.setState({
          txtEmail: '',
          txtTitle: '',
          txtContent: ''
        })
      } else {
        showNotification('Đã có lỗi xảy ra !')
      }
    } else {
      showNotification('Tiêu đề và Nội dung không được để rỗng')
    }
  }

  render () {
    const path = [
      {
        path: '/',
        title: 'Liên hệ'
      }
    ]
    const { txtContent, txtEmail, txtTitle } = this.state
    return (
      <div className='wallet-container'>
        <Breadcrumb path={path} />

        <Row >
          <Col xs={24} md={24} xl={{ offset: 6, span: 12 }} style={{ padding: '1rem' }}>
            <div className='wallet-card'>
              <h4 className='wallet-card__title'>
                Liên hệ với chúng tôi
              </h4>
              <div className='align-center'>
                <Form.Item
                  label='Email'
                  className='my-input my-input--purple'
                >
                  <Input placeholder='Nhập email' name={'txtEmail'} value={txtEmail} onChange={this.onChangeValue()} />
                </Form.Item>
              </div>
              <div className='align-center'>
                <Form.Item
                  label='Tiêu đề'
                  className='my-input my-input--purple'
                >
                  <Input placeholder='Nhập tiêu đề' name={'txtTitle'} value={txtTitle} onChange={this.onChangeValue()} />
                </Form.Item>
              </div>
              <div className='align-center'>
                <Form.Item
                  label='Nội dung'
                  className='my-input my-input--purple'

                >
                  <Input.TextArea
                    placeholder='Nhập nội dung'
                    name={'txtContent'}
                    onChange={this.onChangeValue()}
                    autoSize={{ minRows: 10, maxRows: 15 }}
                    value={txtContent}
                  />
                </Form.Item>
                <div className='button-container'>
                  <MyButton onClick={this.onSubmit()} className='ref-card__btn'>Xác nhận</MyButton>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default HelpAndSupport

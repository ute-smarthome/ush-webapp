import React from 'react'
import { connect } from 'react-redux'
import Breadcrumb from 'pages/Components/Breadcrumb'
import { Row, Col, Spin } from 'antd'
import FilterNavi from './Components/FilterNavi'
import LeaderboardListItem from './Components/LeaderboardListItem'
import './style.scss'
import { images } from 'config/images'
import BaseAPI from 'controller/API/BaseAPI'
import { getLength, generateId } from 'common/function'
import { keyArea } from 'common/constants'
import { withRouter } from 'next/router'

class LeaderboardScreen extends React.PureComponent {
  static getInitialProps ({ query }) {
    return { query }
  }

  constructor (props) {
    super(props)
    this.state = {
      selectedFilter: 'rzFFzJKkdUuODOqm'
    }
  }

  componentDidMount () {
    this.setState({ isLoading: true })
    const { userRedux, router } = this.props
    if (!userRedux) router.push('/authentication')
  }

  handleChangeFilter = (selectedFilter) => {
    console.log(selectedFilter)
    this.setState({ selectedFilter })
  }

  renderListCard = () => {
    const { deviceRedux, userRedux } = this.props
    const { selectedFilter } = this.state
    const { data } = deviceRedux
    const dataRender = selectedFilter === keyArea.all ? data
      : selectedFilter === keyArea.share ? data.filter(item => item.createdUser !== userRedux.id)
        : data.filter(item => item.areaId === selectedFilter)

    return (
      <Row gutter={[32, 32]}>
        {dataRender && dataRender.map(item => {
          return (
            <Col xs={24} md={6} xl={4} xxl={3} lg={6} key={item}>
              <LeaderboardListItem item={item} />
            </Col>
          )
        })}
      </Row>
    )
  }

  render () {
    const { messages } = this.props.locale
    const { deviceRedux, userRedux } = this.props
    const { data } = deviceRedux
    console.log('render -> deviceRedux', deviceRedux)
    const { areaList } = deviceRedux
    const { selectedFilter } = this.state
    const path = [
      {
        path: '/',
        title: 'Trang Chủ'
      }
    ]
    const filterList = areaList && areaList.reduce((arr, item) => {
      if (data.some(itm => itm.areaId === item.id)) {
        arr.push({ key: item.id, title: item.name })
      }
      return arr
    }, [])
    filterList && filterList.unshift({ key: keyArea.all, title: 'Tất cả' }, { key: keyArea.share, title: 'Chia sẻ' })

    return userRedux ? (
      <div className='ldb-container'>
        <Breadcrumb path={path} />
        <div className='x-container' style={{ margin: '0 5rem' }}>
          {/* Filterbar */}
          <Row className='ldb-filter-container'>
            <Col xs={24} md={24} lg={24}>
              <FilterNavi
                selectedKey={selectedFilter}
                list={filterList}
                onChange={this.handleChangeFilter}
              />
            </Col>
            {/* <Col xs={4} md={4} lg={4} className='flex direction-column'>
              <div className='ldb-filter__icon'>
                <img src={images.icFilter} alt='' />
              </div>
            </Col> */}
          </Row>

          {/* CardList */}
          {this.renderListCard()}
        </div>
      </div>
    ) : ''
  }
}

const mapStateToProps = (state) => ({
  userRedux: state.userRedux,
  locale: state.locale,
  deviceRedux: state.deviceRedux,
  areaRedux: state.areaRedux
})

export default connect(mapStateToProps)(withRouter(LeaderboardScreen))

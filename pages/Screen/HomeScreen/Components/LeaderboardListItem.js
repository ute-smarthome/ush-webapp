import React from 'react'
import ReduxServices from 'common/redux'
import Icon, { ClockCircleOutlined, SettingOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons'
import { Row, Col, Modal } from 'antd'
import Light from './deviceComponent/Light'
import Fan from './deviceComponent/Fan'
import AnDevice from './deviceComponent/Andevice'
import LightDimmer from './deviceComponent/LightDimmer'
import Error from './deviceComponent/Error'
import { withRouter } from 'next/router'
import { get } from 'lodash'
import { connect } from 'react-redux'
import { showNotification } from 'common/function'
import BaseAPI from 'controller/API/BaseAPI'
const LeaderboardListItem = (props) => {
  const changeStatus = item => () => {
    console.log('LeaderboardListItem -> item', item)
    const payload = {
      key: item.key,
      data: {
        type: 'controllDevice',
        deviceId: item.id,
        value: 0,
        status: !item.lastStatus
      }
    }
    ReduxServices.updateDeviceRedux(payload)
    ReduxServices.emitSocket(payload)
  }

  const blockDevice = () => () => {
    showNotification('Device has blocked')
  }

  const onChangeValue = (value, item) => () => {
    console.log('onChangeValue -> value, item', value, item)
    if (value) {
      item.lastValue = item.lastValue + 1
    } else {
      item.lastValue = item.lastValue - 1
    }
    if (item.lastValue < 0 || item.lastValue > 3) {
      return
    }
    const payload = {
      key: item.key,
      data: {
        type: 'controllDevice',
        deviceId: item.id,
        value: item.lastValue,
        status: item.lastStatus
      }
    }
    ReduxServices.updateDeviceRedux(payload)
    ReduxServices.emitSocket(payload)
  }

  const renderIcon = () => {
    const { item, userRedux } = props
    const id = get(userRedux, 'id')
    return item.signalTypeId === '1' ? (
      <Light onChangeStatus={(!get(item, 'blockDevice') || (get(item, 'createdUser') === id)) ? changeStatus(item) : blockDevice()} item={item} />
    ) : item.signalTypeId === '2' ? (
      <Fan onChangeValue={onChangeValue} onChangeStatus={(!get(item, 'blockDevice') || (get(item, 'createdUser') === id)) ? changeStatus(item) : blockDevice()} item={item} />
    ) : item.signalTypeId === '3' ? (
      <LightDimmer onChangeValue={onChangeValue} onChangeStatus={(!get(item, 'blockDevice') || (get(item, 'createdUser') === id)) ? changeStatus(item) : blockDevice()} item={item} />
    ) : item.signalTypeId === '4' ? (
      <AnDevice onChangeStatus={(!get(item, 'blockDevice') || (get(item, 'createdUser') === id)) ? changeStatus(item) : blockDevice()} item={item} />
    ) : ''
  }

  const renderError = () => {
    const { item } = props
    return <Error item={item} onSetting={onSetting(item)} />
  }
  const onSetting = (item) => () => {
    const { id } = item
    const { router } = props
    router.push(`/setting/${id}`)
  }

  const onBlockDevice = (item) => async () => {
    console.log('onBlockDevice -> item', item)
    const data = await BaseAPI.putData('device/block', { deviceId: item.id, blockDevice: !item.blockDevice })
    if (data) {
      const payload = {
        type: 'updateBlockDevice',
        key: data.key,
        data: data
      }
      ReduxServices.updateBlockDeviceRedux(data)
      ReduxServices.emitSocket(payload)
      showNotification(data.blockDevice ? 'Block device successful' : 'Unblock device successful')
    }
  }

  const { item, userRedux } = props
  console.log('LeaderboardListItem -> item', item.lastStatus)

  return !item.powerOn ? (
    <div className={`ldb-list-item ldb-list-item__powerOff`}>
      <Row style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center' }} >
        <h4>{get(item, 'name')}</h4>
      </Row>
      <Row style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'space-around', padding: '2rem' }}>
        {renderError()}
      </Row>
      <Row style={{ justifyContent: 'space-evenly', fontSize: '1.6rem' }}>
        Mất kết nối
      </Row>
    </div>
  ) : (
    <div className={`ldb-list-item ldb-list-item__${get(item, 'lastStatus')}`}>
      <Row style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center' }} >
        <h4>{get(item, 'name')}</h4>
      </Row>
      <Row style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'space-around', padding: '2rem' }}>
        {renderIcon()}
      </Row>
      <Row style={{ justifyContent: 'space-evenly' }}>
        <SettingOutlined onClick={onSetting(item)} style={{ fontSize: '20px' }} />
        {
          ['2', '3'].includes(get(item, 'signalTypeId')) ? (
            <div style={{ fontSize: '15px' }}>{get(item, 'lastValue')}</div>
          ) : ''
        }
        {
          get(userRedux, 'id') === get(item, 'createdUser') ? (
            <Icon
              component={get(item, 'blockDevice') ? LockOutlined : UnlockOutlined}
              style={{ fontSize: '20px' }}
              onClick={onBlockDevice(item)}
            />
          ) : ''
        }
        {/* <ClockCircleOutlined style={{ fontSize: '20px' }} /> */}
      </Row>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    userRedux: state.userRedux,
    deviceRedux: state.deviceRedux
  }
}

export default connect(mapStateToProps)(withRouter(LeaderboardListItem))

import React from 'react'
import { get } from 'lodash'

const FilterNavi = ({ list = [], selectedKey = null, onChange = () => { } }) => {
  const onFilterChange = (key) => () => {
    onChange(key)
  }
  const renderList = () => {
    return list.map((item) => {
      return (
        <li
          className={get(item, 'key') === selectedKey && 'active'}
          key={get(item, 'key')}
          onClick={onFilterChange(get(item, 'key'))}
        >
          {get(item, 'title')}
        </li>
      )
    })
  }

  return (
    <div className='ldb-filter-nav'>
      <ul>{renderList()}</ul>
    </div>
  )
}

export default FilterNavi

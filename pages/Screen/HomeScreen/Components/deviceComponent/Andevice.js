import React from 'react'
import { colorIcon } from 'common/constants'
import { faFan, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
class Andevice extends React.PureComponent {
  render () {
    const { item, onChangeStatus } = this.props
    const text = item.lastValue <= 12 ? `${item.lastValue} Tốt`
      : item.lastValue <= 35 ? `${item.lastValue} Tương đối`
        : item.lastValue <= 55 ? `${item.lastValue} Nhạy cảm` : `${item.lastValue} Không tốt`

    return (
        <>
          <div style={{ maxWidth: '100%', fontSize: '12px', color: colorIcon[item.lastStatus], textAlign: 'center' }} > {text}</div>
          {/* <FontAwesomeIcon icon={faMinus} style={{ maxWidth: '100%', fontSize: '12x', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
          <FontAwesomeIcon onClick={onChangeStatus} icon={faFan} style={{ maxWidth: '100%', fontSize: '36px', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
          <FontAwesomeIcon icon={faPlus} style={{ maxWidth: '100%', fontSize: '12px', color: colorIcon[item.lastStatus], textAlign: 'center' }} /> */}
        </>
    )
  }
}

export default Andevice

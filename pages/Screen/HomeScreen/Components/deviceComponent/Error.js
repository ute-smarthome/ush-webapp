import React from 'react'
import { faCogs } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
class Error extends React.PureComponent {
  render () {
    const { onSetting } = this.props
    return (
      <FontAwesomeIcon onClick={onSetting} icon={faCogs} style={{ fontSize: '36px', color: 'black', textAlign: 'center' }} />
    )
  }
}

export default Error

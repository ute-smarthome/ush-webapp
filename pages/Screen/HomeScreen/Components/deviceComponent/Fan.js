import React from 'react'
import { colorIcon } from 'common/constants'
import { faFan, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
class Fan extends React.PureComponent {
  render () {
    const { item, onChangeStatus, onChangeValue } = this.props
    return (
        <>
          <FontAwesomeIcon onClick={onChangeValue(false, item)} icon={faMinus} style={{ maxWidth: '100%', fontSize: '12x', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
          <FontAwesomeIcon onClick={onChangeStatus} icon={faFan} style={{ maxWidth: '100%', fontSize: '36px', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
          <FontAwesomeIcon onClick={onChangeValue(true, item)} icon={faPlus} style={{ maxWidth: '100%', fontSize: '12px', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
        </>
    )
  }
}

export default Fan

import React from 'react'
import { faLightbulb, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'
import { colorIcon } from 'common/constants'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
class LightDimmer extends React.PureComponent {
  render () {
    const { item, onChangeStatus, onChangeValue } = this.props
    return (
        <>
          <FontAwesomeIcon onClick={onChangeValue(false, item)} icon={faMinus} style={{ maxWidth: '100%', fontSize: '12x', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
          <FontAwesomeIcon onClick={onChangeStatus} icon={faLightbulb} style={{ fontSize: '36px', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
          <FontAwesomeIcon onClick={onChangeValue(true, item)} icon={faPlus} style={{ maxWidth: '100%', fontSize: '12px', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
      </>
    )
  }
}

export default LightDimmer

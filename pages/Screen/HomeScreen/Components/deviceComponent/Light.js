import React from 'react'
import { faLightbulb } from '@fortawesome/free-solid-svg-icons'
import { colorIcon } from 'common/constants'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
class Light extends React.PureComponent {
  render () {
    const { item, onChangeStatus } = this.props
    return (
      <FontAwesomeIcon onClick={onChangeStatus} icon={faLightbulb} style={{ fontSize: '36px', color: colorIcon[item.lastStatus], textAlign: 'center' }} />
    )
  }
}

export default Light

import React from 'react'
import { withRouter } from 'next/router'
import BaseAPI from 'controller/API/BaseAPI'
import { getLength } from 'common/function'

class VerifyScreen extends React.PureComponent {
  static getInitialProps ({ query }) {
    return { query }
  }

  constructor (props) {
    super(props)
    this.state = {
    }
  }

  async componentDidMount () {
    this.setState({ isLoading: true })
    const { query, router } = this.props
    const { email, code } = query
    console.log('componentDidMount -> email, code', email, code)
    if (getLength(email) > 0 && getLength(code) > 0) {
      const payload = await BaseAPI.getData(`user/reg/verify?email=${email}&code=${code}`)
      if (payload) { return router.push('/authentication') }
    }
    return router.push('/error')
  }

  render () {
    return (
        <>

        </>
    )
  }
}

export default (withRouter(VerifyScreen))

import React from 'react'
import './style.scss'
import Breadcrumb from 'pages/Components/Breadcrumb'
import { connect } from 'react-redux'
import { Row, Col, Form, Input, Spin } from 'antd'
import MyButton from 'pages/Components/MyButton'
import BaseAPI from 'controller/API/BaseAPI'
import { get } from 'lodash'
import RefListItem from './Components/RefListItem'
import { showNotification } from 'common/function'
import StorageActions from 'controller/Redux/actions/storageActions'
import { bindActionCreators } from 'redux'

class ReferralScreen extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      refBy: null,
      refList: [],
      isFirstLoad: true
    }
  }

  async componentDidMount () {
    this.loadInitial()
  }

  loadInitial = async () => {
    const { userRedux } = this.props

    const idList = userRedux.refList.map((item) => item.id)

    if (userRedux.refBy) idList.push(userRedux.refBy.id)

    const response = await BaseAPI.postData('user/getByListRef', {
      idList
    })

    if (response) {
      const refBy =
        response.find(
          (item) => item.refId === get(userRedux, 'refBy.id', null)
        ) || {}
      let refList =
        response.filter(
          (item) => item.refId !== get(userRedux, 'refBy.id', null)
        ) || []
      refList = userRedux.refList.slice().map((item, index) => {
        item.user = refList[index]
        return item
      })
      this.setState({
        refBy: { ...userRedux.refBy, user: refBy },
        refList,
        isFirstLoad: false
      })
    }
  };

  onConfirmRefId = async ({ refId }) => {
    const { userRedux, setUser, locale } = this.props
    const { messages } = locale

    if (refId === userRedux.refId) {
      return showNotification(messages.error, messages.referDuplicateError)
    }

    this.setState({ isLoading: true })

    const response = await BaseAPI.putData('user/ref/update', { refId })

    this.setState({ isLoading: false })
    if (response) {
      if (response.errMess) {
        return showNotification(messages.error, messages[response.errMess])
      }
      setUser(response)
      this.loadInitial()
    }
  }

  onCopy = (content) => () => {
    const { messages } = this.props.locale
    navigator.clipboard.writeText(content).then(() => {
      showNotification(messages.success, messages.yourContentIsCopied)
    })
  }

  renderRefList = () => {
    const { refList } = this.state
    return refList.map((item) => <RefListItem key={item.id} item={item} />)
  };

  render () {
    const { messages } = this.props.locale
    const { userRedux } = this.props
    const { refBy, isFirstLoad } = this.state
    const path = [
      {
        path: '/',
        title: 'Trang Chủ'
      },
      {
        path: '/referral',
        title: messages.referral
      }
    ]

    // if (isFirstLoad) return <Spin />

    return (
      <div className='referral-container'>
        <Breadcrumb path={path} />
        <div className='container'>
          <Row gutter={32}>
            <Col xs={24} lg={8}>
              <div className='ref-card'>
                <h2 className='ref-card__title'>{messages.referral}</h2>
                <div className='ref-card__content'>
                  <Form name='confirmRefId' onFinish={this.onConfirmRefId}>
                    {refBy ? (
                      <RefListItem item={refBy} />
                    ) : (
                      <Form.Item
                        label={messages.refId}
                        name='refId'
                        className='my-input my-input--purple'
                      >
                        <Input placeholder={messages.refId} />
                      </Form.Item>
                    )}
                  </Form>
                  <Form name='newCustomReferral'>
                    <div className='flex justify-space-between align-center'>
                      <Form.Item
                        label={messages.referralLinks}
                        className='my-input my-input--blue flex'
                      >
                        <Input placeholder={messages.referralLinks} disabled value={`http://ftx.com/#a=${get(userRedux, 'refId')}`} />
                      </Form.Item>
                      <MyButton onClick={this.onCopy(`http://ftx.com/#a=${get(userRedux, 'refId')}`)} className='ref-card__btn'>{messages.copy}</MyButton>
                    </div>
                    <div className='flex justify-space-between align-center'>
                      <Form.Item
                        label={messages.newCustomReferral}
                        className='my-input my-input--purple'
                      >
                        <Input placeholder={messages.newCustomReferral} />
                      </Form.Item>
                      <MyButton className='ref-card__btn'>{messages.create}</MyButton>
                    </div>
                  </Form>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={8}>
              <div className='ref-card'>
                <div className='ref-card__title'>{messages.fee}</div>
                <div className='ref-card__content'>
                  <div className='fee-card'>
                    <ul>
                      <li>
                        <div className='fee-card__title'>{messages.marketFee}</div>
                        <div className='fee-card__content fee-card__content--bold'>0.019000%</div>
                      </li>
                      <li>
                        <div className='fee-card__title'>{messages.takerFee}</div>
                        <div className='fee-card__content'>0.019000%</div>
                      </li>
                      <li>
                        <div className='fee-card__title'>{messages.feeTier}</div>
                        <div className='fee-card__content'>0.019000%</div>
                      </li>
                      <li>
                        <div className='fee-card__title'>{messages.fttBalance}</div>
                        <div className='fee-card__content'>0.019000%</div>
                      </li>
                      <li>
                        <div className='fee-card__title'>{messages['30DayVolume']}</div>
                        <div className='fee-card__content'>0.019000%</div>
                      </li>
                      <li>
                        <div className='fee-card__title'>{messages['30DayMakerVolume']}</div>
                        <div className='fee-card__content'>0.019000%</div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={8}>
              <div className='ref-card'>
                <div className='ref-card__title'>{messages.referralList}</div>
                <div className='ref-card__content'>{this.renderRefList()}</div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userRedux: state.userRedux,
  locale: state.locale
})

const mapDispatchToProps = (dispatch) => ({
  setUser: bindActionCreators(StorageActions.setUserRedux, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ReferralScreen)

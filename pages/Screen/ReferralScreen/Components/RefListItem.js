import React from 'react'
import { get } from 'lodash'
import { convertDateMoment } from 'common/function'
import { images } from 'config/images'
const RefListItem = ({ item }) => {
  const avatar = get(item, ['user', 'image']) ? process.env.REACT_APP_IMAGE + get(item, ['user', 'image']) : images.defaultUser
  return (
    <div className='ref-list-item'>
      <div className='ref-list-item__avatar' style={{ backgroundImage: `url(${avatar})` }} />
      <div className='ref-list-item__info'>
        <div className='ref-list-item__info__name'>{get(item, ['user', 'name'])}</div>
        <div className='ref-list-item__info__date'>{convertDateMoment(get(item, ['date'], new Date()))} </div>
      </div>
    </div>
  )
}

export default RefListItem

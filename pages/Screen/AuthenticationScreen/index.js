import React from 'react'
import { Row, Col } from 'antd'
import { motion } from 'framer-motion'

import SignIn from './Components/SignIn'
import SignUp from './Components/SignUp'
import ForgotPassword from './Components/ForgotPassword'
import MyButton from 'pages/Components/MyButton'
import './style.scss'
import { images } from 'config/images'
import { connect } from 'react-redux'
import BaseAPI from 'controller/API/BaseAPI'
import ReduxServices from 'common/redux'
import { withRouter } from 'next/router'
import { showNotification } from 'common/function'

class Authentication extends React.PureComponent {
  static getInitialProps = ({ query }) => {
    return { query }
  }
  constructor (props) {
    super(props)
    this.state = {
      isSignIn: true,
      isForgotPassword: false
    }
    this.signupForm = React.createRef()
    this.signinForm = React.createRef()
    this.forgotPasswordForm = React.createRef()
  }

  componentDidMount () {
    const { query, router, userRedux } = this.props
    this.setState({ isSignIn: query.action !== 'signUp' })
    if (userRedux) router.push('/')
  }

  handleChangeTab = (isSignIn) => () => {
    this.setState({ isSignIn, isForgotPassword: false })
  }
  handleChangeTabForgot = (isForgotPassword) => () => {
    console.log(isForgotPassword)
    this.setState({ isForgotPassword })
  }

  handleButtonClickSubmit = (isSignIn, isForgotpassword) => () => {
    if (isForgotpassword) {
      return this.forgotPasswordForm.current.submit()
    } else {
      if (isSignIn) {
        return this.signinForm.current.submit()
      }
      return this.signupForm.current.submit()
    }
  }

  handleSubmitSignIn = async ({ email, password, isLogin = true }) => {
    const { router } = this.props
    const { messages } = this.props.locale
    const response = await BaseAPI.postData('user/reg/pw', { email, password, isLogin })
    if (response) {
      if (response.errMess) {
        return showNotification(messages.error, messages[response.errMess])
      }
      ReduxServices.loginUser(response)
      router.push('/')
      return showNotification(messages.success, messages.signInSuccess)
    }

    return showNotification(messages.error, messages.errorPleaseTryAgain)
  }

  handleSubmitForgotPassword = async ({ email }) => {
    const response = await BaseAPI.postData('user/pw/reset', { email })
    if (response) {
      if (!response.success) return showNotification(response.message)
      showNotification(response.message)
      setTimeout(() => {
        this.setState({
          isSignIn: true, isForgotPassword: false
        })
      }, 2000)
    }
  }

  handleSubmitSignUp = async ({ email, password }) => {
    const { router } = this.props
    const { messages } = this.props.locale
    const response = await BaseAPI.postData('user/reg/pw', { email, password })
    if (response) {
      console.log('handleSubmitSignUp -> response', response)
      if (response.errMess) {
        return showNotification(messages.error, messages[response.errMess])
      } else if (response.success) {
        // ReduxServices.loginUser(response)
        router.push('/')
        return showNotification(response.message)
      }
    } else {
      showNotification('Đã có lỗi xảy ra !')
    }
  }
  // xs sm md lg
  render () {
    const layout = {
      xs: {
        span: 24,
        offset: 0
      },
      md: {
        span: 24,
        offset: 0
      },
      lg: { span: 12, offset: 12 },
      xl: { span: 10, offset: 14 },
      xxl: { span: 8, offset: 16 }
    }
    const { messages } = this.props.locale
    const { isSignIn, isForgotPassword } = this.state

    return (
      <div
        className='authentication-container'
        style={{ backgroundImage: `url(${images.imgLoginCover})` }}
      >
        <Row>
          <Col {...layout} className='authentication-form'>
            <div className='authentication-header'>
              <div className='authentication-header__logo'>
                <img src={images.logo} alt='USH LOGO' />
              </div>
              {
                isForgotPassword ? (
                  <ul className='authentication-header-tabs'>
                    <li
                      className='authentication-header-tabs__item'
                    >
                    Quên mật khẩu
                    </li>
                  </ul>
                ) : (
                  <>
                    <ul className='authentication-header-tabs'>
                      <li
                        className='authentication-header-tabs__item'
                        onClick={this.handleChangeTab(true)}
                      >
                    Đăng nhập
                      </li>
                      <li
                        className='authentication-header-tabs__item'
                        onClick={this.handleChangeTab(false)}
                      >
                    Đăng kí
                      </li>
                    </ul>
                    <motion.div
                      className='motion-path'
                      initial={{
                        left: '14%'
                      }}
                      animate={{ left: isSignIn ? '15%' : '65%' }}
                    />
                </>
                )
              }

            </div>
            { isForgotPassword ? (
              <ForgotPassword
                myRef={this.forgotPasswordForm}
                handleSubmitForgotPassword={this.handleSubmitForgotPassword}
                onBackToLogin={this.handleChangeTab(true)}
              />
            ) : isSignIn ? (
              <SignIn
                myRef={this.signinForm}
                handleSubmitSignIn={this.handleSubmitSignIn}
                onForgotPassword={this.handleChangeTabForgot(true)}
              />
            ) : (
              <SignUp
                myRef={this.signupForm}
                handleSubmitSignUp={this.handleSubmitSignUp}
              />
            )}
            <div className='authentication-footer'>
              <MyButton
                onClick={
                  isForgotPassword ? this.handleButtonClickSubmit(false, true)
                    : isSignIn
                      ? this.handleButtonClickSubmit(true, false)
                      : this.handleButtonClickSubmit(false, false)
                }
                title={isForgotPassword ? 'Xác nhận' : isSignIn ? 'Đăng nhập' : 'Đăng kí'}
              />
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  locale: state.locale,
  userRedux: state.userRedux
})

export default withRouter(connect(mapStateToProps)(Authentication))

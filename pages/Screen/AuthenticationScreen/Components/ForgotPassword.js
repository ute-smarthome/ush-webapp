import React from 'react'
import { Form, Input } from 'antd'
import { connect } from 'react-redux'
import Link from 'next/link'

const ForgotPassword = (props) => {
  const { handleSubmitForgotPassword, onBackToLogin } = props
  return (
    <Form name='signInForm' className='form' onFinish={handleSubmitForgotPassword} scrollToFirstError ref={props.myRef}>
      <Form.Item
        ref={props.ref}
        label='Email'
        name='email'
        rules={[
          { required: true, message: 'Nhập email' },
          { type: 'email', message: 'Nhập đúng định dạng email' }
        ]}
        className='my-input'
      >
        <Input type='text' placeholder={'Nhập email'} />
      </Form.Item>
      <a onClick={onBackToLogin}>Về trang đăng nhập</a>
      <button htmlType='submit' style={{ display: 'none' }}>Xác nhận</button>
    </Form>
  )
}

const mapStateToProps = (state) => ({
  locale: state.locale
})

export default connect(mapStateToProps)(ForgotPassword)

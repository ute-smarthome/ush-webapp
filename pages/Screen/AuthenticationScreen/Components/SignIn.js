import React from 'react'
import { Form, Input } from 'antd'
import { connect } from 'react-redux'
import Link from 'next/link'

const SignIn = (props) => {
  const { handleSubmitSignIn, onForgotPassword } = props
  const { messages } = props.locale
  return (
    <Form name='signInForm' className='form' onFinish={handleSubmitSignIn} scrollToFirstError ref={props.myRef}>
      <Form.Item
        ref={props.ref}
        label='Email'
        name='email'
        rules={[
          { required: true, message: 'Nhập email' },
          { type: 'email', message: 'Nhập đúng định dạng email' }
        ]}
        className='my-input'
      >
        <Input type='text' placeholder={'Nhập email'} />
      </Form.Item>
      <Form.Item
        label='Mật khẩu'
        name='password'
        rules={[{ required: true, message: 'Nhập mật khẩu' }]}
        className='my-input'
      >
        <Input type='password' placeholder={'Nhập mật khẩu'} />
      </Form.Item>
      <a onClick={onForgotPassword} >{'Quên mât khẩu'}</a>
      <button htmlType='submit' style={{ display: 'none' }}>Xác nhận</button>
    </Form>
  )
}

const mapStateToProps = (state) => ({
  locale: state.locale
})

export default connect(mapStateToProps)(SignIn)

import React from 'react'
import { Form, Input } from 'antd'
import { connect } from 'react-redux'
import Link from 'next/link'

const SignUp = (props) => {
  const { handleSubmitSignUp } = props
  const { messages } = props.locale
  console.log(props)

  return (
    <Form
      name='signInForm'
      className='form'
      onFinish={handleSubmitSignUp}
      scrollToFirstError
      ref={props.myRef}
    >
      <Form.Item
        label={messages.email}
        name='email'
        rules={[
          { required: true, message: messages.pleaseEnterEmail },
          { type: 'email', message: messages.pleaseEnterCorrectEmail }
        ]}
        className='my-input my-input--blue'
      >
        <Input type='email' placeholder={'Nhập email'} />
      </Form.Item>
      <Form.Item
        label={'Mật khẩu'}
        name='password'
        rules={[{ required: true, message: 'Nhập mật khẩu !' }]}
        className='my-input my-input--gray'
      >
        <Input
          type='password'
          className='gray'
          placeholder={'Nhập mật khẩu'}
        />
      </Form.Item>
      <Form.Item
        label={'Nhập lại mật khẩu'}
        name='confirmPassword'
        rules={[
          { required: true, message: 'Nhập mật khẩu !' }
        ]}
        className='my-input my-input--gray'
      >
        <Input
          type='password'
          className='gray'
          placeholder={'Nhập mật khẩu'}
        />
      </Form.Item>
      <Link to='/'>{messages.bySignUpYouAgree}</Link>
      <button htmlType='submit' style={{ display: 'none' }}>
        submit
      </button>
    </Form>
  )
}

const mapStateToProps = (state) => ({
  locale: state.locale
})

export default connect(mapStateToProps)(SignUp)

import React from 'react'
import Resizer from 'react-image-file-resizer'
import { Row, Col, Form, Input, Label } from 'antd'
import { connect } from 'react-redux'
import { images } from 'config/images'
import Breadcrumb from 'pages/Components/Breadcrumb'
import { get } from 'lodash'
import MyButton from 'pages/Components/MyButton'
import './style.scss'
import { withRouter } from 'next/router'
import BaseAPI from 'controller/API/BaseAPI'
import { bindActionCreators } from 'redux'
import StorageActions from 'controller/Redux/actions/storageActions'
import { getLength, showNotification } from 'common/function'

class AccountScreen extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      typing: false,
      txtFullName: '',
      txtPassword: '',
      txtConfirm: ''
    }

    this.profile = React.createRef()

    this.profileForm = React.createRef()
  }
  handleChangeRoute = (pathname, query = {}) => () => {
    const { router } = this.props
    return router.push({
      pathname,
      query
    })
  }

  handleUpdateUser = async (value) => {
    const { setUser } = this.props
    const response = await BaseAPI.putData('user', value)
    if (response) {
      // Update user here to redux
      console.log('test', response)
      setUser(response)
    }
  }

  handleInputChange = () => (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  handleChangeAvatar = async () => {
    const { setUser } = this.props
    const file = this.profile.current.files[0]
    if (file) {
      const resizeImage = () => {
        return new Promise(async (resolve, reject) => {
          Resizer.imageFileResizer(
            file,
            1024,
            768,
            'PNG',
            70,
            0,
            (uri) => {
              resolve(uri)
            },
            'base64'
          )
        })
      }

      const f = await resizeImage()

      const image = await BaseAPI.postData('upload', {
        base64: f
      })

      const user = await BaseAPI.putData('user', {
        image
      })

      if (user) { setUser(user) }
    }
  }

  onSubmit = () => async () => {
    const { txtFullName, txtPassword, txtConfirm } = this.state
    console.log('onSubmit -> txtFullName, txtPassword, txtConfirm', txtFullName, txtPassword, txtConfirm)
    const body = {}
    if (getLength(txtPassword) > 0) {
      if (txtPassword === txtConfirm) {
        body.password = txtPassword
      } else {
        return showNotification('Mật khẩu mới không khớp !')
      }
    }
    if (getLength(txtFullName) > 0) {
      body.fullName = txtFullName
    }

    if (body) {
      console.log('onSubmit -> body', body)
      const payload = await BaseAPI.putData('user', body)
      if (payload) {
        showNotification('Cập nhập thành công')
        StorageActions.setUserRedux(payload)
        this.setState({
          txtConfirm: '',
          txtPassword: ''
        })
      } else {
        showNotification('Đã có lỗi xảy ra !')
      }
    } else {
      showNotification('Không thể cập nhập rỗng !')
    }
  }

  render () {
    const { userRedux } = this.props
    const initialValues = {
      txtFullName: get(userRedux, 'fullName'),
      email: get(userRedux, 'email')
    }
    const path = [
      {
        path: '/',
        title: 'Trang Chủ'
      },
      {
        path: '/account',
        title: 'Tài khoản'
      }
    ]
    const { txtPassword, txtConfirm } = this.state
    return (
      <div className='account-container'>
        <Breadcrumb path={path} />

        <Row className='user-info-container'>
          <Col xs={10} offset={7}>
            <div className='wallet-card'>

              <Row>

                <Col xs={8}>
                  <div className='user-info__avatar'>
                    <img
                      src={
                        get(userRedux, ['image'])
                          ? `${process.env.REACT_APP_IMAGE}${get(userRedux, [
                            'image'
                          ])}`
                          : images.defaultUser
                      }
                      alt=''
                    />
                    <input ref={this.profile} onChange={this.handleChangeAvatar} type='file' />
                  </div>
                </Col>

                <Col xs={16}>
                  <h4 className='wallet-card__title'>
                  Thông tin cá nhân
                  </h4>
                  <Form ref={this.profileForm} className='profileForm' initialValues={initialValues}>
                    <Form.Item
                      label='Email'
                      name='email'
                      className='my-input'
                    >
                      <Input disabled />
                    </Form.Item>
                    <Form.Item
                      label={'Tên'}
                      name='txtFullName'
                      className='my-input'
                    >
                      <Input placeholder='Nhập tên' name='txtFullName' onChange={this.handleInputChange()} />
                    </Form.Item>

                  </Form>
                  <Form ref={this.profileForm} className='profileForm' >
                    <Form.Item
                      label={'Mật khẩu mới'}
                      className='my-input'
                    >
                      <Input placeholder='Nhập password' value={txtPassword} name='txtPassword' type='password' onChange={this.handleInputChange()} />
                    </Form.Item>
                    <Form.Item
                      label={'Nhập lại'}
                      className='my-input'
                    >
                      <Input placeholder='Nhập password' value={txtConfirm} name='txtConfirm' type='password' onChange={this.handleInputChange()} />
                    </Form.Item>
                    <div className='button-container'>
                      <MyButton onClick={this.onSubmit()} className='ref-card__btn'>Cập nhập</MyButton>
                    </div>
                  </Form>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userRedux: state.userRedux,
  locale: state.locale
})

const mapDispatchToProps = (dispatch) => ({
  setUser: bindActionCreators(StorageActions.setUserRedux, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AccountScreen))

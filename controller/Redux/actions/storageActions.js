import { KEY_STORE } from 'common/constants'
import { saveDataLocal, updateDataLocal } from 'common/function'

export default class StorageActions {
  static setLocale (payload) {
    saveDataLocal(KEY_STORE.SET_LOCALE, payload)
    return {
      type: KEY_STORE.SET_LOCALE,
      payload
    }
  }

  static setUserRedux (payload) {
    saveDataLocal(KEY_STORE.SET_USER, payload)
    return {
      type: KEY_STORE.SET_USER,
      payload
    }
  }

  static setTicker (payload) {
    saveDataLocal(KEY_STORE.SET_STICKER, payload)
    return {
      type: KEY_STORE.SET_STICKER,
      payload
    }
  }

  static setSetting (payload) {
    saveDataLocal(KEY_STORE.SET_SETTING, payload)
    return {
      type: KEY_STORE.SET_SETTING,
      payload
    }
  }

  static setDevice (payload) {
    saveDataLocal(KEY_STORE.SET_DEVICE, payload)
    return {
      type: KEY_STORE.SET_DEVICE,
      payload
    }
  }

  static setArea (payload) {
    saveDataLocal(KEY_STORE.SET_AREA, payload)
    return {
      type: KEY_STORE.SET_AREA,
      payload
    }
  }
}

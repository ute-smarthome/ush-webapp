import createReducer from '../lib/reducerConfig'
import MessageJA from 'static/Assets/Lang/ja.json'
import MessageEN from 'static/Assets/Lang/en.json'
import MessageCN from 'static/Assets/Lang/cn.json'
import MessageVI from 'static/Assets/Lang/vi.json'
import { KEY_STORE } from 'common/constants'
import initState from '../lib/initState'
const localeJA = {
  lang: 'ja',
  messages: MessageJA
}

const localeEN = {
  lang: 'en',
  messages: MessageEN
}

const localeCN = {
  lang: 'cn',
  messages: MessageCN
}

const localeVI = {
  lang: 'vi',
  messages: MessageVI
}

export const locale = createReducer(localeEN, {
  [KEY_STORE.SET_LOCALE] (state, action) {
    switch (action.payload) {
    case 'en':
      return localeEN
    case 'ja':
      return localeJA
    case 'cn':
      return localeCN
    case 'vi':
      return localeVI
    default:
      return localeEN
    }
  }
})

export const tickerRedux = createReducer(initState.arrayEmpty, {
  [KEY_STORE.SET_STICKER] (state, action) {
    return action.payload
  }
})

export const userRedux = createReducer(initState.objNull, {
  [KEY_STORE.SET_USER] (state, action) {
    return action.payload
  }
})

export const settingRedux = createReducer(initState.objNull, {
  [KEY_STORE.SET_SETTING] (state, action) {
    return action.payload
  }
})

export const headerRedux = createReducer(initState.header, {
  [KEY_STORE.SET_HEADER] (state, action) {
    return action.payload
  }
})

export const deviceRedux = createReducer(initState.objEmpty, {
  [KEY_STORE.SET_DEVICE] (state, action) {
    return action.payload
  }
})

export const areaRedux = createReducer(initState.arrayEmpty, {
  [KEY_STORE.SET_AREA] (state, action) {
    return action.payload
  }
})

import storeRedux from 'controller/Redux/store/configureStore'
import PageReduxAction from 'controller/Redux/actions/pageActions'
import { showNotification, saveDataLocal, removeDataLocal, generateId } from './function'
import { getDataLocal } from 'common/function'
import StorageActions from 'controller/Redux/actions/storageActions'
import { KEY_STORE, typeDevicePower } from './constants'
import init from 'controller/Redux/lib/initState'
import BaseAPI from 'controller/API/BaseAPI'
import SocketIOClient from 'socket.io-client/dist/socket.io.js'
import { get } from 'lodash'

let socketService
export default class ReduxServices {
  static async refreshInternet (isConnect, isChange) {
    const { locale } = storeRedux.getState()
    const { messages } = locale
    if (isConnect) {
      isChange && showNotification(messages.warnInternerOnline)
    } else {
      showNotification(messages.warnInternerOffline)
    }

    ReduxServices.callDispatchAction(PageReduxAction.setInternet(isConnect))
  }

  static async refreshDevice () {
    const resSetting = await BaseAPI.getData('device')
    if (resSetting) {
      ReduxServices.callDispatchAction(StorageActions.setDevice(resSetting))
    }
  }

  static async refreshArea () {
    const resSetting = await BaseAPI.getData('area')
    if (resSetting) {
      ReduxServices.callDispatchAction(StorageActions.setArea(resSetting))
    }
  }

  static async loginUser (resLogin) {
    if (resLogin) {
      saveDataLocal(KEY_STORE.JWT_TOKEN, resLogin.jwtToken)
      ReduxServices.callDispatchAction(StorageActions.setUserRedux(resLogin.data))
      ReduxServices.refreshDevice()
      ReduxServices.activeSocket()
    }
  }

  static async activeSocket (isReconnect = true) {
    const { userRedux } = storeRedux.getState()
    const userID = userRedux ? userRedux.id : generateId()
    try {
      if (socketService) {
        socketService.removeAllListeners()
        socketService.disconnect()
        socketService = null
      }
      setTimeout(() => {
        if (isReconnect) {
          socketService = SocketIOClient(`${process.env.REACT_APP_SOCKET}?token=${getDataLocal(KEY_STORE.JWT_TOKEN)}`, {
            transports: ['websocket']
          })

          socketService.on('connect', async () => {
            socketService.emit('userConnect', userID)
          })
          socketService.on('fmx_socketList', (event) => {
            this.callDispatchAction(StorageActions.setTicker(event))
          })

          socketService.on('disconnect', (e) => {
            // logDebug('Disconnect socket')
            console.log('socket disconnected')
            this.activeSocket(true)
          })
          socketService.on('message', event => {
            if (get(event, 'payload.data.type') === 'controllDevice') {
              const { payload } = event
              ReduxServices.updateDeviceRedux(payload)
            }
            if (get(event, 'payload.data.type') === 'sensorUpdate') {
              console.log(event.payload.data)
            }
            if (get(event, 'payload.type') === 'updateBlockDevice') {
              ReduxServices.updateBlockDeviceRedux(get(event, 'payload.data'))
            }
            if ([typeDevicePower.online, typeDevicePower.offline].includes(get(event, 'payload.type'))) {
              const { payload } = event
              ReduxServices.updateOnlineDeviceRedux(payload)
            }
            if (get(event, 'payload.data.type') === 'resetDevice') {
              const { payload } = event
              ReduxServices.deleteDeviceReset(payload)
            }
          })
        }
      }, 500)
    } catch (error) {
      this.activesocket(true)
    }
  }

  static emitSocket (payload) {
    socketService.emit('message', payload)
  }

  static async resetReduxData () {
    removeDataLocal(KEY_STORE.JWT_TOKEN)
    removeDataLocal(KEY_STORE.SET_AREA)
    removeDataLocal(KEY_STORE.SET_DEVICE)
    const storageRedux = [
      { action: StorageActions.setUserRedux, init: init.objNull }

    ]
    storageRedux.forEach(itm => {
      this.callDispatchAction(itm.action(itm.init))
    })
  }

  static updateDeviceRedux (payload) {
    const { deviceRedux } = storeRedux.getState()
    const findIndex = deviceRedux && deviceRedux.data.findIndex(item => get(payload, 'data.deviceId') === item.id)
    if (findIndex !== -1) {
      deviceRedux.data[findIndex].lastStatus = payload.data.status
      deviceRedux.data[findIndex].lastValue = payload.data.value
      deviceRedux.data[findIndex].name = payload.data.name || deviceRedux.data[findIndex].name
      ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
    }
  }

  static updateBlockDeviceRedux (data) {
    const { deviceRedux } = storeRedux.getState()
    const findIndex = deviceRedux && deviceRedux.data.findIndex(item => item.id === data.id)
    if (findIndex !== -1) {
      deviceRedux.data[findIndex].blockDevice = data.blockDevice
      ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
    }
  }
  static updateOnlineDeviceRedux (data) {
    const { deviceRedux } = storeRedux.getState()
    deviceRedux && deviceRedux.data.map((item, index) => {
      if (item.keyId === data.id) {
        item.powerOn = data.type === typeDevicePower.online
      }
    })
    ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
  }

  static updateShareDeviceRedux (data) {
    const { deviceRedux } = storeRedux.getState()
    const findIndex = deviceRedux && deviceRedux.data.findIndex(item => item.id === data.id)
    if (findIndex !== -1) {
      deviceRedux.data[findIndex].userId = data.userId
      ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
    }
  }

  static updateAreaDeviceRedux (data) {
    const { deviceRedux } = storeRedux.getState()
    const findIndex = deviceRedux && deviceRedux.data.findIndex(item => item.id === data.id)
    if (findIndex !== -1) {
      deviceRedux.data[findIndex].areaName = data.areaName
      deviceRedux.data[findIndex].areaId = data.areaId
      ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
    }
  }

  static deleteDeviceReset (payload) {
    const { deviceRedux } = storeRedux.getState()
    const newData = deviceRedux.data.filter(item => item.keyId !== payload.data.keyId)
    deviceRedux.data = newData
    ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
  }

  static addNewAreaDeviceRedux (data) {
    const { deviceRedux } = storeRedux.getState()
    if (deviceRedux) {
      deviceRedux.areaList.push(data)
      ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
    }
  }

  static deleteAreaDeviceRedux (data) {
    const { deviceRedux } = storeRedux.getState()
    const findIndex = deviceRedux && deviceRedux.areaList.findIndex(item => item.id === data.id)
    if (findIndex !== -1) {
      deviceRedux.areaList.splice(findIndex, 1)
      ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
    }
  }

  static deleteDeviceRedux (data) {
    const { deviceRedux } = storeRedux.getState()

    deviceRedux.data.reduce((list, device, index) => {
      if (device.keyId === data.id) list.push(index)
      return list
    }, []).reverse().forEach((index) => {
      deviceRedux.data.splice(index, 1)
    })
    ReduxServices.callDispatchAction(StorageActions.setDevice({ ...deviceRedux }))
  }

  static async callDispatchAction (action) {
    storeRedux.dispatch(action)
  }
}

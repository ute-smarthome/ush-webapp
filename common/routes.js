const routes = require('next-routes')

module.exports = routes()
  // FTX Routes
  .add('/authentication', 'Screen/AuthenticationScreen')
  .add('/account', 'Screen/AccountScreen')
  .add('/leaderboard', 'Screen/LeaderboardScreen')
  .add('/manager', 'Screen/ManagerDeviceScreen')
  .add('/referral', 'Screen/ReferralScreen')
  .add('/setting', 'pages/_error')
  .add('/setting/:id', 'Screen/SettingScreen')
  .add('/helpAndSupport', 'Screen/HelpAndSupport')
  .add('/verify', 'Screen/Verify')
  // main routes
  .add('/home', 'Screen/HomeScreen')
  .add('/home/en', 'Screen/HomeScreen')
  .add('/home/ja', 'Screen/HomeScreen')
  .add('/home/cn', 'Screen/HomeScreen')
  .add('/error', 'pages/_error')

"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

var _Object$defineProperty2 = require("@babel/runtime-corejs2/core-js/object/define-property");

_Object$defineProperty2(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/define-property"));

var _defineProperties = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/define-properties"));

var _getOwnPropertyDescriptors = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/get-own-property-descriptors"));

var _getOwnPropertyDescriptor = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/get-own-property-descriptor"));

var _getOwnPropertySymbols = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/get-own-property-symbols"));

var _keys = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/keys"));

var _defineProperty3 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _configureStore = _interopRequireDefault(require("../controller/Redux/store/configureStore"));

var _pageActions = _interopRequireDefault(require("../controller/Redux/actions/pageActions"));

var _function = require("./function");

var _storageActions = _interopRequireDefault(require("../controller/Redux/actions/storageActions"));

var _constants = require("./constants");

var _initState = _interopRequireDefault(require("../controller/Redux/lib/initState"));

var _BaseAPI = _interopRequireDefault(require("../controller/API/BaseAPI"));

var _socketIo = _interopRequireDefault(require("socket.io-client/dist/socket.io.js"));

var _lodash = require("lodash");

function ownKeys(object, enumerableOnly) { var keys = (0, _keys["default"])(object); if (_getOwnPropertySymbols["default"]) { var symbols = (0, _getOwnPropertySymbols["default"])(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return (0, _getOwnPropertyDescriptor["default"])(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { (0, _defineProperty3["default"])(target, key, source[key]); }); } else if (_getOwnPropertyDescriptors["default"]) { (0, _defineProperties["default"])(target, (0, _getOwnPropertyDescriptors["default"])(source)); } else { ownKeys(source).forEach(function (key) { (0, _defineProperty2["default"])(target, key, (0, _getOwnPropertyDescriptor["default"])(source, key)); }); } } return target; }

var socketService;

var ReduxServices =
/*#__PURE__*/
function () {
  function ReduxServices() {
    (0, _classCallCheck2["default"])(this, ReduxServices);
  }

  (0, _createClass2["default"])(ReduxServices, null, [{
    key: "refreshInternet",
    value: function () {
      var _refreshInternet = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee(isConnect, isChange) {
        var _storeRedux$getState, locale, messages;

        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _storeRedux$getState = _configureStore["default"].getState(), locale = _storeRedux$getState.locale;
                messages = locale.messages;

                if (isConnect) {
                  isChange && (0, _function.showNotification)(messages.warnInternerOnline);
                } else {
                  (0, _function.showNotification)(messages.warnInternerOffline);
                }

                ReduxServices.callDispatchAction(_pageActions["default"].setInternet(isConnect));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function refreshInternet(_x, _x2) {
        return _refreshInternet.apply(this, arguments);
      }

      return refreshInternet;
    }()
  }, {
    key: "refreshDevice",
    value: function () {
      var _refreshDevice = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2() {
        var resSetting;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _BaseAPI["default"].getData('device');

              case 2:
                resSetting = _context2.sent;

                if (resSetting) {
                  ReduxServices.callDispatchAction(_storageActions["default"].setDevice(resSetting));
                }

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function refreshDevice() {
        return _refreshDevice.apply(this, arguments);
      }

      return refreshDevice;
    }()
  }, {
    key: "refreshArea",
    value: function () {
      var _refreshArea = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee3() {
        var resSetting;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return _BaseAPI["default"].getData('area');

              case 2:
                resSetting = _context3.sent;

                if (resSetting) {
                  ReduxServices.callDispatchAction(_storageActions["default"].setArea(resSetting));
                }

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function refreshArea() {
        return _refreshArea.apply(this, arguments);
      }

      return refreshArea;
    }()
  }, {
    key: "loginUser",
    value: function () {
      var _loginUser = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee4(resLogin) {
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (resLogin) {
                  (0, _function.saveDataLocal)(_constants.KEY_STORE.JWT_TOKEN, resLogin.jwtToken);
                  ReduxServices.callDispatchAction(_storageActions["default"].setUserRedux(resLogin.data));
                  ReduxServices.refreshDevice();
                  ReduxServices.activeSocket();
                }

              case 1:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function loginUser(_x3) {
        return _loginUser.apply(this, arguments);
      }

      return loginUser;
    }()
  }, {
    key: "activeSocket",
    value: function () {
      var _activeSocket = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee6() {
        var _this = this;

        var isReconnect,
            _storeRedux$getState2,
            userRedux,
            userID,
            _args6 = arguments;

        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                isReconnect = _args6.length > 0 && _args6[0] !== undefined ? _args6[0] : true;
                _storeRedux$getState2 = _configureStore["default"].getState(), userRedux = _storeRedux$getState2.userRedux;
                userID = userRedux ? userRedux.id : (0, _function.generateId)();

                try {
                  if (socketService) {
                    socketService.removeAllListeners();
                    socketService.disconnect();
                    socketService = null;
                  }

                  setTimeout(function () {
                    if (isReconnect) {
                      socketService = (0, _socketIo["default"])("".concat(process.env.REACT_APP_SOCKET, "?token=").concat((0, _function.getDataLocal)(_constants.KEY_STORE.JWT_TOKEN)), {
                        transports: ['websocket']
                      });
                      socketService.on('connect',
                      /*#__PURE__*/
                      (0, _asyncToGenerator2["default"])(
                      /*#__PURE__*/
                      _regenerator["default"].mark(function _callee5() {
                        return _regenerator["default"].wrap(function _callee5$(_context5) {
                          while (1) {
                            switch (_context5.prev = _context5.next) {
                              case 0:
                                socketService.emit('userConnect', userID);

                              case 1:
                              case "end":
                                return _context5.stop();
                            }
                          }
                        }, _callee5);
                      })));
                      socketService.on('fmx_socketList', function (event) {
                        _this.callDispatchAction(_storageActions["default"].setTicker(event));
                      });
                      socketService.on('disconnect', function (e) {
                        // logDebug('Disconnect socket')
                        console.log('socket disconnected');

                        _this.activeSocket(true);
                      });
                      socketService.on('message', function (event) {
                        console.log('ReduxServices -> activeSocket -> event', event);

                        if ((0, _lodash.get)(event, 'payload.data.type') === 'controllDevice') {
                          var payload = event.payload;
                          ReduxServices.updateDeviceRedux(payload);
                        }

                        if ((0, _lodash.get)(event, 'payload.type') === 'updateBlockDevice') {
                          ReduxServices.updateBlockDeviceRedux((0, _lodash.get)(event, 'payload.data'));
                        }
                      });
                    }
                  }, 500);
                } catch (error) {
                  this.activesocket(true);
                }

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function activeSocket() {
        return _activeSocket.apply(this, arguments);
      }

      return activeSocket;
    }()
  }, {
    key: "emitSocket",
    value: function emitSocket(payload) {
      socketService.emit('message', payload);
    }
  }, {
    key: "resetReduxData",
    value: function () {
      var _resetReduxData = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee7() {
        var _this2 = this;

        var storageRedux;
        return _regenerator["default"].wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                (0, _function.removeDataLocal)(_constants.KEY_STORE.JWT_TOKEN);
                (0, _function.removeDataLocal)(_constants.KEY_STORE.SET_AREA);
                (0, _function.removeDataLocal)(_constants.KEY_STORE.SET_DEVICE);
                storageRedux = [{
                  action: _storageActions["default"].setUserRedux,
                  init: _initState["default"].objNull
                }];
                storageRedux.forEach(function (itm) {
                  _this2.callDispatchAction(itm.action(itm.init));
                });

              case 5:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }));

      function resetReduxData() {
        return _resetReduxData.apply(this, arguments);
      }

      return resetReduxData;
    }()
  }, {
    key: "updateDeviceRedux",
    value: function updateDeviceRedux(payload) {
      var _storeRedux$getState3 = _configureStore["default"].getState(),
          deviceRedux = _storeRedux$getState3.deviceRedux;

      var findIndex = deviceRedux && deviceRedux.data.findIndex(function (item) {
        return (0, _lodash.get)(payload, 'data.deviceId') === item.id;
      });

      if (findIndex !== -1) {
        deviceRedux.data[findIndex].lastStatus = payload.data.status;
        deviceRedux.data[findIndex].lastValue = payload.data.value;
        ReduxServices.callDispatchAction(_storageActions["default"].setDevice(_objectSpread({}, deviceRedux)));
      }
    }
  }, {
    key: "updateBlockDeviceRedux",
    value: function updateBlockDeviceRedux(data) {
      var _storeRedux$getState4 = _configureStore["default"].getState(),
          deviceRedux = _storeRedux$getState4.deviceRedux;

      var findIndex = deviceRedux && deviceRedux.data.findIndex(function (item) {
        return item.id === data.id;
      });

      if (findIndex !== -1) {
        deviceRedux.data[findIndex].blockDevice = data.blockDevice;
        ReduxServices.callDispatchAction(_storageActions["default"].setDevice(_objectSpread({}, deviceRedux)));
      }
    }
  }, {
    key: "updateShareDeviceRedux",
    value: function updateShareDeviceRedux(data) {
      var _storeRedux$getState5 = _configureStore["default"].getState(),
          deviceRedux = _storeRedux$getState5.deviceRedux;

      var findIndex = deviceRedux && deviceRedux.data.findIndex(function (item) {
        return item.id === data.id;
      });

      if (findIndex !== -1) {
        deviceRedux.data[findIndex].userId = data.userId;
        ReduxServices.callDispatchAction(_storageActions["default"].setDevice(_objectSpread({}, deviceRedux)));
      }
    }
  }, {
    key: "updateAreaDeviceRedux",
    value: function updateAreaDeviceRedux(data) {
      var _storeRedux$getState6 = _configureStore["default"].getState(),
          deviceRedux = _storeRedux$getState6.deviceRedux;

      var findIndex = deviceRedux && deviceRedux.data.findIndex(function (item) {
        return item.id === data.id;
      });

      if (findIndex !== -1) {
        deviceRedux.data[findIndex].areaName = data.areaName;
        deviceRedux.data[findIndex].areaId = data.areaId;
        ReduxServices.callDispatchAction(_storageActions["default"].setDevice(_objectSpread({}, deviceRedux)));
      }
    }
  }, {
    key: "addNewAreaDeviceRedux",
    value: function addNewAreaDeviceRedux(data) {
      var _storeRedux$getState7 = _configureStore["default"].getState(),
          deviceRedux = _storeRedux$getState7.deviceRedux;

      if (deviceRedux) {
        deviceRedux.areaList.push(data);
        ReduxServices.callDispatchAction(_storageActions["default"].setDevice(_objectSpread({}, deviceRedux)));
      }
    }
  }, {
    key: "callDispatchAction",
    value: function () {
      var _callDispatchAction = (0, _asyncToGenerator2["default"])(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee8(action) {
        return _regenerator["default"].wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _configureStore["default"].dispatch(action);

              case 1:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }));

      function callDispatchAction(_x4) {
        return _callDispatchAction.apply(this, arguments);
      }

      return callDispatchAction;
    }()
  }]);
  return ReduxServices;
}();

exports["default"] = ReduxServices;